{
  "_exclude": [
    "age",
    "uptime",
    "last_change",
    "cksum",
    "seq",
    "dead_timer",
    "hello_timer",
    "checksum",
    "seq_num",
    "statistics",
    "lsas",
    "last_state_change",
    "bdr_ip_addr",
    "dr_ip_addr",
    "state",
    "bdr_router_id",
    "dr_router_id",
    "area_scope_lsa_cksum_sum"
  ],
  "attributes": null,
  "commands": null,
  "connections": null,
  "context_manager": {},
  "info": {
    "vrf": {
      "default": {
        "address_family": {
          "ipv4": {
            "instance": {
              "1": {
                "adjacency_stagger": {
                  "initial_number": 300,
                  "maximum_number": 300
                },
                "areas": {
                  "0.0.0.20": {
                    "area_id": "0.0.0.20",
                    "area_type": "normal",
                    "database": {
                      "lsa_types": {
                        "1": {
                          "lsa_type": 1,
                          "lsas": {
                            "0.0.0.3 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "0.0.0.3",
                              "ospfv2": {
                                "body": {
                                  "router": {
                                    "links": {
                                      "192.1.35.4": {
                                        "link_data": "192.1.35.3",
                                        "link_id": "192.1.35.4",
                                        "topologies": {
                                          "0": {
                                            "metric": 10,
                                            "mt_id": 0
                                          }
                                        },
                                        "type": "transit network"
                                      },
                                      "33.0.0.0": {
                                        "link_data": "255.0.0.0",
                                        "link_id": "33.0.0.0",
                                        "topologies": {
                                          "0": {
                                            "metric": 1,
                                            "mt_id": 0
                                          }
                                        },
                                        "type": "stub network"
                                      }
                                    },
                                    "num_of_links": 2
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 708,
                                  "checksum": "0xF528",
                                  "length": 48,
                                  "lsa_id": "0.0.0.3",
                                  "option": "None",
                                  "seq_num": "80000003",
                                  "type": 1
                                }
                              }
                            },
                            "0.0.0.5 0.0.0.5": {
                              "adv_router": "0.0.0.5",
                              "lsa_id": "0.0.0.5",
                              "ospfv2": {
                                "body": {
                                  "router": {
                                    "links": {
                                      "192.1.35.4": {
                                        "link_data": "192.1.35.4",
                                        "link_id": "192.1.35.4",
                                        "topologies": {
                                          "0": {
                                            "metric": 10,
                                            "mt_id": 0
                                          }
                                        },
                                        "type": "transit network"
                                      },
                                      "192.1.56.6": {
                                        "link_data": "192.1.56.4",
                                        "link_id": "192.1.56.6",
                                        "topologies": {
                                          "0": {
                                            "metric": 10,
                                            "mt_id": 0
                                          }
                                        },
                                        "type": "transit network"
                                      },
                                      "205.1.4.0": {
                                        "link_data": "255.255.255.0",
                                        "link_id": "205.1.4.0",
                                        "topologies": {
                                          "0": {
                                            "metric": 1,
                                            "mt_id": 0
                                          }
                                        },
                                        "type": "stub network"
                                      },
                                      "205.1.5.0": {
                                        "link_data": "255.255.255.0",
                                        "link_id": "205.1.5.0",
                                        "topologies": {
                                          "0": {
                                            "metric": 1,
                                            "mt_id": 0
                                          }
                                        },
                                        "type": "stub network"
                                      },
                                      "205.1.6.0": {
                                        "link_data": "255.255.255.0",
                                        "link_id": "205.1.6.0",
                                        "topologies": {
                                          "0": {
                                            "metric": 1,
                                            "mt_id": 0
                                          }
                                        },
                                        "type": "stub network"
                                      },
                                      "205.1.7.0": {
                                        "link_data": "255.255.255.0",
                                        "link_id": "205.1.7.0",
                                        "topologies": {
                                          "0": {
                                            "metric": 1,
                                            "mt_id": 0
                                          }
                                        },
                                        "type": "stub network"
                                      },
                                      "5.0.0.0": {
                                        "link_data": "255.0.0.0",
                                        "link_id": "5.0.0.0",
                                        "topologies": {
                                          "0": {
                                            "metric": 1,
                                            "mt_id": 0
                                          }
                                        },
                                        "type": "stub network"
                                      },
                                      "55.0.0.0": {
                                        "link_data": "255.0.0.0",
                                        "link_id": "55.0.0.0",
                                        "topologies": {
                                          "0": {
                                            "metric": 1,
                                            "mt_id": 0
                                          }
                                        },
                                        "type": "stub network"
                                      }
                                    },
                                    "num_of_links": 8
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.5",
                                  "age": 698,
                                  "checksum": "0xF44A",
                                  "length": 120,
                                  "lsa_id": "0.0.0.5",
                                  "option": "None",
                                  "seq_num": "80000005",
                                  "type": 1
                                }
                              }
                            },
                            "0.0.0.6 0.0.0.6": {
                              "adv_router": "0.0.0.6",
                              "lsa_id": "0.0.0.6",
                              "ospfv2": {
                                "body": {
                                  "router": {
                                    "links": {
                                      "192.1.56.6": {
                                        "link_data": "192.1.56.6",
                                        "link_id": "192.1.56.6",
                                        "topologies": {
                                          "0": {
                                            "metric": 10,
                                            "mt_id": 0
                                          }
                                        },
                                        "type": "transit network"
                                      },
                                      "6.0.0.0": {
                                        "link_data": "255.0.0.0",
                                        "link_id": "6.0.0.0",
                                        "topologies": {
                                          "0": {
                                            "metric": 1,
                                            "mt_id": 0
                                          }
                                        },
                                        "type": "stub network"
                                      }
                                    },
                                    "num_of_links": 2
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.6",
                                  "age": 705,
                                  "checksum": "0x47BC",
                                  "length": 48,
                                  "lsa_id": "0.0.0.6",
                                  "option": "None",
                                  "seq_num": "80000004",
                                  "type": 1
                                }
                              }
                            }
                          }
                        },
                        "2": {
                          "lsa_type": 2,
                          "lsas": {
                            "192.1.35.4 0.0.0.5": {
                              "adv_router": "0.0.0.5",
                              "lsa_id": "192.1.35.4",
                              "ospfv2": {
                                "body": {
                                  "network": {
                                    "attached_routers": {
                                      "0.0.0.3": {},
                                      "0.0.0.5": {}
                                    },
                                    "network_mask": "255.255.255.0"
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.5",
                                  "age": 715,
                                  "checksum": "0x5DE6",
                                  "length": 32,
                                  "lsa_id": "192.1.35.4",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 2
                                }
                              }
                            },
                            "192.1.56.6 0.0.0.6": {
                              "adv_router": "0.0.0.6",
                              "lsa_id": "192.1.56.6",
                              "ospfv2": {
                                "body": {
                                  "network": {
                                    "attached_routers": {
                                      "0.0.0.5": {},
                                      "0.0.0.6": {}
                                    },
                                    "network_mask": "255.255.255.0"
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.6",
                                  "age": 711,
                                  "checksum": "0x81A7",
                                  "length": 32,
                                  "lsa_id": "192.1.56.6",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 2
                                }
                              }
                            }
                          }
                        },
                        "3": {
                          "lsa_type": 3,
                          "lsas": {
                            "1.0.0.0 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "1.0.0.0",
                              "ospfv2": {
                                "body": {
                                  "summary": {
                                    "network_mask": "255.0.0.0",
                                    "topologies": {
                                      "0": {
                                        "metric": 11,
                                        "mt_id": 0
                                      }
                                    }
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 703,
                                  "checksum": "0xD855",
                                  "length": 28,
                                  "lsa_id": "1.0.0.0",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 3
                                }
                              }
                            },
                            "10.0.0.0 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "10.0.0.0",
                              "ospfv2": {
                                "body": {
                                  "summary": {
                                    "network_mask": "255.0.0.0",
                                    "topologies": {
                                      "0": {
                                        "metric": 41,
                                        "mt_id": 0
                                      }
                                    }
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 703,
                                  "checksum": "0x9076",
                                  "length": 28,
                                  "lsa_id": "10.0.0.0",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 3
                                }
                              }
                            },
                            "100.0.0.0 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "100.0.0.0",
                              "ospfv2": {
                                "body": {
                                  "summary": {
                                    "network_mask": "255.0.0.0",
                                    "topologies": {
                                      "0": {
                                        "metric": 41,
                                        "mt_id": 0
                                      }
                                    }
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 703,
                                  "checksum": "0xF9B2",
                                  "length": 28,
                                  "lsa_id": "100.0.0.0",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 3
                                }
                              }
                            },
                            "192.1.12.0 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "192.1.12.0",
                              "ospfv2": {
                                "body": {
                                  "summary": {
                                    "network_mask": "255.255.255.0",
                                    "topologies": {
                                      "0": {
                                        "metric": 20,
                                        "mt_id": 0
                                      }
                                    }
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 703,
                                  "checksum": "0xE572",
                                  "length": 28,
                                  "lsa_id": "192.1.12.0",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 3
                                }
                              }
                            },
                            "192.1.13.0 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "192.1.13.0",
                              "ospfv2": {
                                "body": {
                                  "summary": {
                                    "network_mask": "255.255.255.0",
                                    "topologies": {
                                      "0": {
                                        "metric": 10,
                                        "mt_id": 0
                                      }
                                    }
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 753,
                                  "checksum": "0x76EA",
                                  "length": 28,
                                  "lsa_id": "192.1.13.0",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 3
                                }
                              }
                            },
                            "192.1.24.0 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "192.1.24.0",
                              "ospfv2": {
                                "body": {
                                  "summary": {
                                    "network_mask": "255.255.255.0",
                                    "topologies": {
                                      "0": {
                                        "metric": 30,
                                        "mt_id": 0
                                      }
                                    }
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 703,
                                  "checksum": "0xC57C",
                                  "length": 28,
                                  "lsa_id": "192.1.24.0",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 3
                                }
                              }
                            },
                            "192.1.40.0 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "192.1.40.0",
                              "ospfv2": {
                                "body": {
                                  "summary": {
                                    "network_mask": "255.255.255.0",
                                    "topologies": {
                                      "0": {
                                        "metric": 40,
                                        "mt_id": 0
                                      }
                                    }
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 703,
                                  "checksum": "0x79AE",
                                  "length": 28,
                                  "lsa_id": "192.1.40.0",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 3
                                }
                              }
                            },
                            "2.0.0.0 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "2.0.0.0",
                              "ospfv2": {
                                "body": {
                                  "summary": {
                                    "network_mask": "255.0.0.0",
                                    "topologies": {
                                      "0": {
                                        "metric": 21,
                                        "mt_id": 0
                                      }
                                    }
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 703,
                                  "checksum": "0x30F2",
                                  "length": 28,
                                  "lsa_id": "2.0.0.0",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 3
                                }
                              }
                            },
                            "201.1.12.0 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "201.1.12.0",
                              "ospfv2": {
                                "body": {
                                  "summary": {
                                    "network_mask": "255.255.255.0",
                                    "topologies": {
                                      "0": {
                                        "metric": 41,
                                        "mt_id": 0
                                      }
                                    }
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 703,
                                  "checksum": "0x43F6",
                                  "length": 28,
                                  "lsa_id": "201.1.12.0",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 3
                                }
                              }
                            },
                            "201.1.13.0 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "201.1.13.0",
                              "ospfv2": {
                                "body": {
                                  "summary": {
                                    "network_mask": "255.255.255.0",
                                    "topologies": {
                                      "0": {
                                        "metric": 41,
                                        "mt_id": 0
                                      }
                                    }
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 703,
                                  "checksum": "0x3801",
                                  "length": 28,
                                  "lsa_id": "201.1.13.0",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 3
                                }
                              }
                            },
                            "201.1.14.0 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "201.1.14.0",
                              "ospfv2": {
                                "body": {
                                  "summary": {
                                    "network_mask": "255.255.255.0",
                                    "topologies": {
                                      "0": {
                                        "metric": 41,
                                        "mt_id": 0
                                      }
                                    }
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 703,
                                  "checksum": "0x2D0B",
                                  "length": 28,
                                  "lsa_id": "201.1.14.0",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 3
                                }
                              }
                            },
                            "201.1.15.0 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "201.1.15.0",
                              "ospfv2": {
                                "body": {
                                  "summary": {
                                    "network_mask": "255.255.255.0",
                                    "topologies": {
                                      "0": {
                                        "metric": 41,
                                        "mt_id": 0
                                      }
                                    }
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 703,
                                  "checksum": "0x2215",
                                  "length": 28,
                                  "lsa_id": "201.1.15.0",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 3
                                }
                              }
                            },
                            "22.0.0.0 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "22.0.0.0",
                              "ospfv2": {
                                "body": {
                                  "summary": {
                                    "network_mask": "255.0.0.0",
                                    "topologies": {
                                      "0": {
                                        "metric": 21,
                                        "mt_id": 0
                                      }
                                    }
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 703,
                                  "checksum": "0x2BE3",
                                  "length": 28,
                                  "lsa_id": "22.0.0.0",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 3
                                }
                              }
                            },
                            "3.0.0.0 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "3.0.0.0",
                              "ospfv2": {
                                "body": {
                                  "summary": {
                                    "network_mask": "255.0.0.0",
                                    "topologies": {
                                      "0": {
                                        "metric": 1,
                                        "mt_id": 0
                                      }
                                    }
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 753,
                                  "checksum": "0x5ADB",
                                  "length": 28,
                                  "lsa_id": "3.0.0.0",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 3
                                }
                              }
                            },
                            "4.0.0.0 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "4.0.0.0",
                              "ospfv2": {
                                "body": {
                                  "summary": {
                                    "network_mask": "255.0.0.0",
                                    "topologies": {
                                      "0": {
                                        "metric": 31,
                                        "mt_id": 0
                                      }
                                    }
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 703,
                                  "checksum": "0x7A9C",
                                  "length": 28,
                                  "lsa_id": "4.0.0.0",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 3
                                }
                              }
                            },
                            "44.0.0.0 0.0.0.3": {
                              "adv_router": "0.0.0.3",
                              "lsa_id": "44.0.0.0",
                              "ospfv2": {
                                "body": {
                                  "summary": {
                                    "network_mask": "255.0.0.0",
                                    "topologies": {
                                      "0": {
                                        "metric": 31,
                                        "mt_id": 0
                                      }
                                    }
                                  }
                                },
                                "header": {
                                  "adv_router": "0.0.0.3",
                                  "age": 703,
                                  "checksum": "0x707E",
                                  "length": 28,
                                  "lsa_id": "44.0.0.0",
                                  "option": "None",
                                  "seq_num": "80000001",
                                  "type": 3
                                }
                              }
                            }
                          }
                        }
                      }
                    },
                    "interfaces": {
                      "Ethernet0/0": {
                        "bdr_ip_addr": "192.1.56.4",
                        "bdr_router_id": "0.0.0.5",
                        "bfd": {
                          "enable": false
                        },
                        "cost": 10,
                        "dead_interval": 40,
                        "demand_circuit": false,
                        "dr_ip_addr": "192.1.56.6",
                        "dr_router_id": "0.0.0.6",
                        "enable": true,
                        "hello_interval": 10,
                        "hello_timer": "00:00:07",
                        "interface_type": "broadcast",
                        "lls": true,
                        "name": "Ethernet0/0",
                        "neighbors": {
                          "0.0.0.5": {
                            "address": "192.1.56.4",
                            "bdr_ip_addr": "192.1.56.4",
                            "dead_timer": "00:00:38",
                            "dr_ip_addr": "192.1.56.6",
                            "neighbor_router_id": "0.0.0.5",
                            "state": "full",
                            "statistics": {
                              "nbr_event_count": 6,
                              "nbr_retrans_qlen": 0
                            }
                          }
                        },
                        "passive": false,
                        "priority": 1,
                        "retransmit_interval": 5,
                        "state": "dr",
                        "transmit_delay": 1
                      },
                      "Loopback0": {
                        "bfd": {
                          "enable": false
                        },
                        "cost": 1,
                        "dead_interval": 40,
                        "demand_circuit": false,
                        "enable": true,
                        "hello_interval": 10,
                        "interface_type": "point-to-point",
                        "lls": true,
                        "name": "Loopback0",
                        "retransmit_interval": 5,
                        "state": "point-to-point",
                        "transmit_delay": 1
                      }
                    },
                    "mpls": {
                      "te": {
                        "enable": false
                      }
                    },
                    "statistics": {
                      "area_scope_lsa_cksum_sum": "0x0A7C9F",
                      "area_scope_lsa_count": 21,
                      "spf_runs_count": 3
                    }
                  }
                },
                "auto_cost": {
                  "enable": false
                },
                "bfd": {
                  "enable": false
                },
                "graceful_restart": {
                  "cisco": {
                    "enable": false,
                    "type": "cisco"
                  },
                  "ietf": {
                    "enable": false,
                    "type": "ietf"
                  }
                },
                "mpls": {
                  "ldp": {
                    "autoconfig": false,
                    "autoconfig_area_id": "0.0.0.20"
                  }
                },
                "nsr": {
                  "enable": false
                },
                "preference": {
                  "single_value": {
                    "all": 110
                  }
                },
                "router_id": "0.0.0.6",
                "spf_control": {
                  "paths": 4,
                  "throttle": {
                    "spf": {
                      "hold": 10000,
                      "maximum": 10000,
                      "start": 5000
                    }
                  }
                },
                "stub_router": {
                  "always": {
                    "always": false,
                    "external_lsa": false,
                    "include_stub": false,
                    "summary_lsa": false
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}