+++ R6: executing command 'show ip ospf' +++
show ip ospf
 Routing Process "ospf 1" with ID 0.0.0.6
 Start time: 00:36:16.842, Time elapsed: 00:12:34.021
 Supports only single TOS(TOS0) routes
 Supports opaque LSA
 Supports Link-local Signaling (LLS)
 Supports area transit capability
 Supports NSSA (compatible with RFC 3101)
 Supports Database Exchange Summary List Optimization (RFC 5243)
 Event-log enabled, Maximum number of events: 1000, Mode: cyclic
 Router is not originating router-LSAs with maximum metric
 Initial SPF schedule delay 5000 msecs
 Minimum hold time between two consecutive SPFs 10000 msecs
 Maximum wait time between two consecutive SPFs 10000 msecs
 Incremental-SPF disabled
 Minimum LSA interval 5 secs
 Minimum LSA arrival 1000 msecs
 LSA group pacing timer 240 secs
 Interface flood pacing timer 33 msecs
 Retransmission pacing timer 66 msecs
 EXCHANGE/LOADING adjacency limit: initial 300, process maximum 300
 Number of external LSA 0. Checksum Sum 0x000000
 Number of opaque AS LSA 0. Checksum Sum 0x000000
 Number of DCbitless external and opaque AS LSA 0
 Number of DoNotAge external and opaque AS LSA 0
 Number of areas in this router is 1. 1 normal 0 stub 0 nssa
 Number of areas transit capable is 0
 External flood list length 0
 IETF NSF helper support enabled
 Cisco NSF helper support enabled
 Reference bandwidth unit is 100 mbps
    Area 20
        Number of interfaces in this area is 2
	Area has no authentication
	SPF algorithm last executed 00:11:38.344 ago
	SPF algorithm executed 3 times
	Area ranges are
	Number of LSA 21. Checksum Sum 0x0A7C9F
	Number of opaque link LSA 0. Checksum Sum 0x000000
	Number of DCbitless LSA 0
	Number of indication LSA 0
	Number of DoNotAge LSA 0
	Flood list length 0

R6#
+++ R6: executing command 'show ip protocols' +++
show ip protocols
*** IP Routing is NSF aware ***

Routing Protocol is "application"
  Sending updates every 0 seconds
  Invalid after 0 seconds, hold down 0, flushed after 0
  Outgoing update filter list for all interfaces is not set
  Incoming update filter list for all interfaces is not set
  Maximum path: 32
  Routing for Networks:
  Routing Information Sources:
    Gateway         Distance      Last Update
  Distance: (default is 4)

Routing Protocol is "ospf 1"
  Outgoing update filter list for all interfaces is not set
  Incoming update filter list for all interfaces is not set
  Router ID 0.0.0.6
  Number of areas in this router is 1. 1 normal 0 stub 0 nssa
  Maximum path: 4
  Routing for Networks:
    6.0.0.0 0.255.255.255 area 20
    192.1.56.0 0.0.0.255 area 20
  Routing Information Sources:
    Gateway         Distance      Last Update
    0.0.0.3              110      00:11:38
    0.0.0.5              110      00:11:38
  Distance: (default is 110)

Routing Protocol is "rip"
  Outgoing update filter list for all interfaces is not set
  Incoming update filter list for all interfaces is not set
  Sending updates every 30 seconds, next due in 23 seconds
  Invalid after 180 seconds, hold down 180, flushed after 240
  Redistributing: rip
  Default version control: send version 2, receive version 2
    Interface                           Send  Recv  Triggered RIP  Key-chain
    Ethernet0/1                         2     2          No        none            
    Loopback11                          2     2          No        none            
  Automatic network summarization is not in effect
  Maximum path: 4
  Routing for Networks:
    66.0.0.0
    192.1.69.0
  Routing Information Sources:
    Gateway         Distance      Last Update
    192.1.69.9           120      00:00:24
  Distance: (default is 120)

R6#
+++ R6: executing command 'show running-config | section router ospf 1' +++
show running-config | section router ospf 1
router ospf 1
 router-id 0.0.0.6
 network 6.0.0.0 0.255.255.255 area 20
 network 192.1.56.0 0.0.0.255 area 20
R6#
+++ R6: executing command 'show ip ospf mpls ldp interface' +++
show ip ospf mpls ldp interface
Loopback0
  Process ID 1, Area 20
  LDP is not configured through LDP autoconfig
  LDP-IGP Synchronization : Not required
  Holddown timer is disabled
  Interface is up 
Ethernet0/0
  Process ID 1, Area 20
  LDP is not configured through LDP autoconfig
  LDP-IGP Synchronization : Not required
  Holddown timer is disabled
  Interface is up 
R6#
+++ R6: executing command 'show ip ospf mpls traffic-eng link' +++
show ip ospf mpls traffic-eng link

            OSPF Router with ID (0.0.0.6) (Process ID 1)

  Area 20 MPLS TE not initialized
R6#
+++ R6: executing command 'show running-config | section router ospf 1' +++
show running-config | section router ospf 1
router ospf 1
 router-id 0.0.0.6
 network 6.0.0.0 0.255.255.255 area 20
 network 192.1.56.0 0.0.0.255 area 20
R6#
+++ R6: executing command 'show ip ospf database router' +++
show ip ospf database router

            OSPF Router with ID (0.0.0.6) (Process ID 1)

		Router Link States (Area 20)

  LS age: 708
  Options: (No TOS-capability, DC)
  LS Type: Router Links
  Link State ID: 0.0.0.3
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000003
  Checksum: 0xF528
  Length: 48
  Area Border Router
  Number of Links: 2

    Link connected to: a Stub Network
     (Link ID) Network/subnet number: 33.0.0.0
     (Link Data) Network Mask: 255.0.0.0
      Number of MTID metrics: 0
       TOS 0 Metrics: 1

    Link connected to: a Transit Network
     (Link ID) Designated Router address: 192.1.35.4
     (Link Data) Router Interface address: 192.1.35.3
      Number of MTID metrics: 0
       TOS 0 Metrics: 10


  LS age: 698
  Options: (No TOS-capability, DC)
  LS Type: Router Links
  Link State ID: 0.0.0.5
  Advertising Router: 0.0.0.5
  LS Seq Number: 80000005
  Checksum: 0xF44A
  Length: 120
  Number of Links: 8

    Link connected to: a Stub Network
     (Link ID) Network/subnet number: 5.0.0.0
     (Link Data) Network Mask: 255.0.0.0
      Number of MTID metrics: 0
       TOS 0 Metrics: 1

    Link connected to: a Stub Network
     (Link ID) Network/subnet number: 55.0.0.0
     (Link Data) Network Mask: 255.0.0.0
      Number of MTID metrics: 0
       TOS 0 Metrics: 1

    Link connected to: a Stub Network
     (Link ID) Network/subnet number: 205.1.4.0
     (Link Data) Network Mask: 255.255.255.0
      Number of MTID metrics: 0
       TOS 0 Metrics: 1

    Link connected to: a Stub Network
     (Link ID) Network/subnet number: 205.1.5.0
     (Link Data) Network Mask: 255.255.255.0
      Number of MTID metrics: 0
       TOS 0 Metrics: 1

    Link connected to: a Stub Network
     (Link ID) Network/subnet number: 205.1.6.0
     (Link Data) Network Mask: 255.255.255.0
      Number of MTID metrics: 0
       TOS 0 Metrics: 1

    Link connected to: a Stub Network
     (Link ID) Network/subnet number: 205.1.7.0
     (Link Data) Network Mask: 255.255.255.0
      Number of MTID metrics: 0
       TOS 0 Metrics: 1

    Link connected to: a Transit Network
     (Link ID) Designated Router address: 192.1.56.6
     (Link Data) Router Interface address: 192.1.56.4
      Number of MTID metrics: 0
       TOS 0 Metrics: 10

    Link connected to: a Transit Network
     (Link ID) Designated Router address: 192.1.35.4
     (Link Data) Router Interface address: 192.1.35.4
      Number of MTID metrics: 0
       TOS 0 Metrics: 10


  LS age: 705
  Options: (No TOS-capability, DC)
  LS Type: Router Links
  Link State ID: 0.0.0.6
  Advertising Router: 0.0.0.6
  LS Seq Number: 80000004
  Checksum: 0x47BC
  Length: 48
  Number of Links: 2

    Link connected to: a Stub Network
     (Link ID) Network/subnet number: 6.0.0.0
     (Link Data) Network Mask: 255.0.0.0
      Number of MTID metrics: 0
       TOS 0 Metrics: 1

    Link connected to: a Transit Network
     (Link ID) Designated Router address: 192.1.56.6
     (Link Data) Router Interface address: 192.1.56.6
      Number of MTID metrics: 0
       TOS 0 Metrics: 10


R6#
+++ R6: executing command 'show ip ospf database network' +++
show ip ospf database network

            OSPF Router with ID (0.0.0.6) (Process ID 1)

		Net Link States (Area 20)

  LS age: 715
  Options: (No TOS-capability, DC)
  LS Type: Network Links
  Link State ID: 192.1.35.4 (address of Designated Router)
  Advertising Router: 0.0.0.5
  LS Seq Number: 80000001
  Checksum: 0x5DE6
  Length: 32
  Network Mask: /24
	Attached Router: 0.0.0.5
	Attached Router: 0.0.0.3

  LS age: 711
  Options: (No TOS-capability, DC)
  LS Type: Network Links
  Link State ID: 192.1.56.6 (address of Designated Router)
  Advertising Router: 0.0.0.6
  LS Seq Number: 80000001
  Checksum: 0x81A7
  Length: 32
  Network Mask: /24
	Attached Router: 0.0.0.6
	Attached Router: 0.0.0.5

R6#
+++ R6: executing command 'show ip ospf database external' +++
show ip ospf database external

            OSPF Router with ID (0.0.0.6) (Process ID 1)
R6#
+++ R6: executing command 'show ip ospf database summary' +++
show ip ospf database summary

            OSPF Router with ID (0.0.0.6) (Process ID 1)

		Summary Net Link States (Area 20)

  LS age: 703
  Options: (No TOS-capability, DC, Upward)
  LS Type: Summary Links(Network)
  Link State ID: 1.0.0.0 (summary Network Number)
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000001
  Checksum: 0xD855
  Length: 28
  Network Mask: /8
	MTID: 0 	Metric: 11 

  LS age: 703
  Options: (No TOS-capability, DC, Upward)
  LS Type: Summary Links(Network)
  Link State ID: 2.0.0.0 (summary Network Number)
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000001
  Checksum: 0x30F2
  Length: 28
  Network Mask: /8
	MTID: 0 	Metric: 21 

  LS age: 753
  Options: (No TOS-capability, DC, Upward)
  LS Type: Summary Links(Network)
  Link State ID: 3.0.0.0 (summary Network Number)
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000001
  Checksum: 0x5ADB
  Length: 28
  Network Mask: /8
	MTID: 0 	Metric: 1 

  LS age: 703
  Options: (No TOS-capability, DC, Upward)
  LS Type: Summary Links(Network)
  Link State ID: 4.0.0.0 (summary Network Number)
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000001
  Checksum: 0x7A9C
  Length: 28
  Network Mask: /8
	MTID: 0 	Metric: 31 

  LS age: 703
  Options: (No TOS-capability, DC, Upward)
  LS Type: Summary Links(Network)
  Link State ID: 10.0.0.0 (summary Network Number)
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000001
  Checksum: 0x9076
  Length: 28
  Network Mask: /8
	MTID: 0 	Metric: 41 

  LS age: 703
  Options: (No TOS-capability, DC, Upward)
  LS Type: Summary Links(Network)
  Link State ID: 22.0.0.0 (summary Network Number)
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000001
  Checksum: 0x2BE3
  Length: 28
  Network Mask: /8
	MTID: 0 	Metric: 21 

  LS age: 703
  Options: (No TOS-capability, DC, Upward)
  LS Type: Summary Links(Network)
  Link State ID: 44.0.0.0 (summary Network Number)
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000001
  Checksum: 0x707E
  Length: 28
  Network Mask: /8
	MTID: 0 	Metric: 31 

  LS age: 703
  Options: (No TOS-capability, DC, Upward)
  LS Type: Summary Links(Network)
  Link State ID: 100.0.0.0 (summary Network Number)
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000001
  Checksum: 0xF9B2
  Length: 28
  Network Mask: /8
	MTID: 0 	Metric: 41 

  LS age: 703
  Options: (No TOS-capability, DC, Upward)
  LS Type: Summary Links(Network)
  Link State ID: 192.1.12.0 (summary Network Number)
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000001
  Checksum: 0xE572
  Length: 28
  Network Mask: /24
	MTID: 0 	Metric: 20 

  LS age: 753
  Options: (No TOS-capability, DC, Upward)
  LS Type: Summary Links(Network)
  Link State ID: 192.1.13.0 (summary Network Number)
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000001
  Checksum: 0x76EA
  Length: 28
  Network Mask: /24
	MTID: 0 	Metric: 10 

  LS age: 703
  Options: (No TOS-capability, DC, Upward)
  LS Type: Summary Links(Network)
  Link State ID: 192.1.24.0 (summary Network Number)
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000001
  Checksum: 0xC57C
  Length: 28
  Network Mask: /24
	MTID: 0 	Metric: 30 

  LS age: 703
  Options: (No TOS-capability, DC, Upward)
  LS Type: Summary Links(Network)
  Link State ID: 192.1.40.0 (summary Network Number)
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000001
  Checksum: 0x79AE
  Length: 28
  Network Mask: /24
	MTID: 0 	Metric: 40 

  LS age: 703
  Options: (No TOS-capability, DC, Upward)
  LS Type: Summary Links(Network)
  Link State ID: 201.1.12.0 (summary Network Number)
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000001
  Checksum: 0x43F6
  Length: 28
  Network Mask: /24
	MTID: 0 	Metric: 41 

  LS age: 703
  Options: (No TOS-capability, DC, Upward)
  LS Type: Summary Links(Network)
  Link State ID: 201.1.13.0 (summary Network Number)
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000001
  Checksum: 0x3801
  Length: 28
  Network Mask: /24
	MTID: 0 	Metric: 41 

  LS age: 703
  Options: (No TOS-capability, DC, Upward)
  LS Type: Summary Links(Network)
  Link State ID: 201.1.14.0 (summary Network Number)
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000001
  Checksum: 0x2D0B
  Length: 28
  Network Mask: /24
	MTID: 0 	Metric: 41 

  LS age: 703
  Options: (No TOS-capability, DC, Upward)
  LS Type: Summary Links(Network)
  Link State ID: 201.1.15.0 (summary Network Number)
  Advertising Router: 0.0.0.3
  LS Seq Number: 80000001
  Checksum: 0x2215
  Length: 28
  Network Mask: /24
	MTID: 0 	Metric: 41 

R6#
+++ R6: executing command 'show ip ospf database opaque-area' +++
show ip ospf database opaque-area

            OSPF Router with ID (0.0.0.6) (Process ID 1)
R6#
+++ R6: executing command 'show ip ospf virtual-links' +++
show ip ospf virtual-links
R6#
+++ R6: executing command 'show ip ospf interface' +++
show ip ospf interface
Loopback0 is up, line protocol is up 
  Internet Address 6.6.6.6/8, Area 20, Attached via Network Statement
  Process ID 1, Router ID 0.0.0.6, Network Type POINT_TO_POINT, Cost: 1
  Topology-MTID    Cost    Disabled    Shutdown      Topology Name
        0           1         no          no            Base
  Transmit Delay is 1 sec, State POINT_TO_POINT
  Timer intervals configured, Hello 10, Dead 40, Wait 40, Retransmit 5
    oob-resync timeout 40
  Supports Link-local Signaling (LLS)
  Cisco NSF helper support enabled
  IETF NSF helper support enabled
  Index 1/2/2, flood queue length 0
  Next 0x0(0)/0x0(0)/0x0(0)
  Last flood scan length is 0, maximum is 0
  Last flood scan time is 0 msec, maximum is 0 msec
  Neighbor Count is 0, Adjacent neighbor count is 0 
  Suppress hello for 0 neighbor(s)
Ethernet0/0 is up, line protocol is up 
  Internet Address 192.1.56.6/24, Area 20, Attached via Network Statement
  Process ID 1, Router ID 0.0.0.6, Network Type BROADCAST, Cost: 10
  Topology-MTID    Cost    Disabled    Shutdown      Topology Name
        0           10        no          no            Base
  Transmit Delay is 1 sec, State DR, Priority 1
  Designated Router (ID) 0.0.0.6, Interface address 192.1.56.6
  Backup Designated router (ID) 0.0.0.5, Interface address 192.1.56.4
  Timer intervals configured, Hello 10, Dead 40, Wait 40, Retransmit 5
    oob-resync timeout 40
    Hello due in 00:00:07
  Supports Link-local Signaling (LLS)
  Cisco NSF helper support enabled
  IETF NSF helper support enabled
  Index 1/1/1, flood queue length 0
  Next 0x0(0)/0x0(0)/0x0(0)
  Last flood scan length is 0, maximum is 1
  Last flood scan time is 0 msec, maximum is 0 msec
  Neighbor Count is 1, Adjacent neighbor count is 1 
    Adjacent with neighbor 0.0.0.5  (Backup Designated Router)
  Suppress hello for 0 neighbor(s)
R6#
+++ R6: executing command 'show running-config | section router ospf 1' +++
show running-config | section router ospf 1
router ospf 1
 router-id 0.0.0.6
 network 6.0.0.0 0.255.255.255 area 20
 network 192.1.56.0 0.0.0.255 area 20
R6#
+++ R6: executing command 'show running-config | section router ospf 1' +++
show running-config | section router ospf 1
router ospf 1
 router-id 0.0.0.6
 network 6.0.0.0 0.255.255.255 area 20
 network 192.1.56.0 0.0.0.255 area 20
R6#
+++ R6: executing command 'show ip ospf neighbor detail' +++
show ip ospf neighbor detail
 Neighbor 0.0.0.5, interface address 192.1.56.4
    In the area 20 via interface Ethernet0/0
    Neighbor priority is 1, State is FULL, 6 state changes
    DR is 192.1.56.6 BDR is 192.1.56.4
    Options is 0x12 in Hello (E-bit, L-bit)
    Options is 0x52 in DBD (E-bit, L-bit, O-bit)
    LLS Options is 0x1 (LR)
    Dead timer due in 00:00:38
    Neighbor is up for 00:12:28
    Index 1/1/1, retransmission queue length 0, number of retransmission 0
    First 0x0(0)/0x0(0)/0x0(0) Next 0x0(0)/0x0(0)/0x0(0)
    Last retransmission scan length is 0, maximum is 0
    Last retransmission scan time is 0 msec, maximum is 0 msec
R6#
+++ R6: executing command 'show ip ospf interface Ethernet0/0' +++
show ip ospf interface Ethernet0/0
Ethernet0/0 is up, line protocol is up 
  Internet Address 192.1.56.6/24, Area 20, Attached via Network Statement
  Process ID 1, Router ID 0.0.0.6, Network Type BROADCAST, Cost: 10
  Topology-MTID    Cost    Disabled    Shutdown      Topology Name
        0           10        no          no            Base
  Transmit Delay is 1 sec, State DR, Priority 1
  Designated Router (ID) 0.0.0.6, Interface address 192.1.56.6
  Backup Designated router (ID) 0.0.0.5, Interface address 192.1.56.4
  Timer intervals configured, Hello 10, Dead 40, Wait 40, Retransmit 5
    oob-resync timeout 40
    Hello due in 00:00:06
  Supports Link-local Signaling (LLS)
  Cisco NSF helper support enabled
  IETF NSF helper support enabled
  Index 1/1/1, flood queue length 0
  Next 0x0(0)/0x0(0)/0x0(0)
  Last flood scan length is 0, maximum is 1
  Last flood scan time is 0 msec, maximum is 0 msec
  Neighbor Count is 1, Adjacent neighbor count is 1 
    Adjacent with neighbor 0.0.0.5  (Backup Designated Router)
  Suppress hello for 0 neighbor(s)
R6#
+++ R6: executing command 'show running-config | section router ospf 1' +++
show running-config | section router ospf 1
router ospf 1
 router-id 0.0.0.6
 network 6.0.0.0 0.255.255.255 area 20
 network 192.1.56.0 0.0.0.255 area 20
R6#
+++ R6: executing command 'show ip ospf sham-links' +++
show ip ospf sham-links
R6#
Could not learn <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfShamLinks'>
Show Command: show ip ospf sham-links
Parser Output is empty
+====================================================================================================================================================+
| Commands for learning feature 'Ospf'                                                                                                               |
+====================================================================================================================================================+
| - Parsed commands                                                                                                                                  |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspf'>                                                                                        |
|   cmd: <class 'genie.libs.parser.iosxe.show_protocols.ShowIpProtocols'>, arguments: {'vrf':''}                                                     |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfMplsLdpInterface'>, arguments: {'interface':''}                                           |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfMplsTrafficEngLink'>                                                                      |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfDatabaseRouter'>                                                                          |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfDatabaseNetwork'>                                                                         |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfDatabaseExternal'>                                                                        |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfDatabaseSummary'>                                                                         |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfDatabaseOpaqueArea'>                                                                      |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfInterface'>, arguments: {'interface':''}                                                  |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfNeighborDetail'>, arguments: {'neighbor':''}                                              |
|====================================================================================================================================================|
| - Commands with empty output                                                                                                                       |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfVirtualLinks'>                                                                            |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfShamLinks'>                                                                               |
|====================================================================================================================================================|
