{
  "_exclude": [
    "in_discards",
    "in_octets",
    "in_pkts",
    "last_clear",
    "out_octets",
    "out_pkts",
    "in_rate",
    "out_rate",
    "in_errors",
    "in_crc_errors",
    "in_rate_pkts",
    "out_rate_pkts",
    "in_broadcast_pkts",
    "out_broadcast_pkts",
    "in_multicast_pkts",
    "out_multicast_pkts",
    "in_unicast_pkts",
    "out_unicast_pkts",
    "last_change",
    "mac_address",
    "phys_address",
    "((t|T)unnel.*)",
    "(Null.*)",
    "chars_out",
    "chars_in",
    "pkts_out",
    "pkts_in",
    "mgmt0"
  ],
  "attributes": null,
  "commands": null,
  "connections": null,
  "context_manager": {},
  "info": {
    "Ethernet0/0": {
      "accounting": {
        "arp": {
          "chars_in": 300,
          "chars_out": 360,
          "pkts_in": 5,
          "pkts_out": 6
        },
        "cdp": {
          "chars_in": 10028,
          "chars_out": 13739,
          "pkts_in": 26,
          "pkts_out": 35
        },
        "dec mop": {
          "chars_in": 154,
          "chars_out": 231,
          "pkts_in": 2,
          "pkts_out": 3
        },
        "ip": {
          "chars_in": 14052,
          "chars_out": 13828,
          "pkts_in": 171,
          "pkts_out": 168
        },
        "other": {
          "chars_in": 120,
          "chars_out": 8520,
          "pkts_in": 2,
          "pkts_out": 142
        }
      },
      "bandwidth": 10000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 196,
        "in_octets": 24534,
        "in_pkts": 204,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 36678,
        "out_pkts": 354,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 1000,
      "duplex_mode": "auto",
      "enabled": true,
      "encapsulation": {
        "encapsulation": "arpa"
      },
      "flow_control": {
        "receive": false,
        "send": false
      },
      "ipv4": {
        "192.1.18.8/24": {
          "ip": "192.1.18.8",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mac_address": "aabb.cc00.4000",
      "mtu": 1500,
      "oper_status": "up",
      "phys_address": "aabb.cc00.4000",
      "port_channel": {
        "port_channel_member": false
      },
      "port_speed": "auto speed",
      "switchport_enable": false,
      "type": "AmdP2"
    },
    "Ethernet0/1": {
      "bandwidth": 10000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 1230,
        "out_pkts": 6,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 1000,
      "duplex_mode": "auto",
      "enabled": false,
      "encapsulation": {
        "encapsulation": "arpa"
      },
      "flow_control": {
        "receive": false,
        "send": false
      },
      "mac_address": "aabb.cc00.4010",
      "mtu": 1500,
      "oper_status": "down",
      "phys_address": "aabb.cc00.4010",
      "port_channel": {
        "port_channel_member": false
      },
      "port_speed": "auto speed",
      "switchport_enable": false,
      "type": "AmdP2"
    },
    "Ethernet0/2": {
      "bandwidth": 10000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 1230,
        "out_pkts": 6,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 1000,
      "duplex_mode": "auto",
      "enabled": false,
      "encapsulation": {
        "encapsulation": "arpa"
      },
      "flow_control": {
        "receive": false,
        "send": false
      },
      "mac_address": "aabb.cc00.4020",
      "mtu": 1500,
      "oper_status": "down",
      "phys_address": "aabb.cc00.4020",
      "port_channel": {
        "port_channel_member": false
      },
      "port_speed": "auto speed",
      "switchport_enable": false,
      "type": "AmdP2"
    },
    "Ethernet0/3": {
      "accounting": {
        "arp": {
          "chars_in": 20580,
          "chars_out": 1980,
          "pkts_in": 343,
          "pkts_out": 33
        },
        "cdp": {
          "chars_in": 43283,
          "chars_out": 4936,
          "pkts_in": 114,
          "pkts_out": 13
        },
        "dec mop": {
          "chars_in": 3542,
          "chars_out": 462,
          "pkts_in": 46,
          "pkts_out": 6
        },
        "ip": {
          "chars_in": 161656,
          "chars_out": 200615,
          "pkts_in": 1484,
          "pkts_out": 1579
        },
        "other": {
          "chars_in": 2667,
          "chars_out": 17460,
          "pkts_in": 10,
          "pkts_out": 291
        }
      },
      "bandwidth": 10000,
      "counters": {
        "in_broadcast_pkts": 100,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 688,
        "in_octets": 229434,
        "in_pkts": 1960,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 209581,
        "out_pkts": 1874,
        "rate": {
          "in_rate": 3000,
          "in_rate_pkts": 3,
          "load_interval": 300,
          "out_rate": 3000,
          "out_rate_pkts": 3
        }
      },
      "delay": 1000,
      "duplex_mode": "auto",
      "enabled": true,
      "encapsulation": {
        "encapsulation": "arpa"
      },
      "flow_control": {
        "receive": false,
        "send": false
      },
      "ipv4": {
        "192.168.20.179/24": {
          "ip": "192.168.20.179",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mac_address": "aabb.cc00.4030",
      "mtu": 1500,
      "oper_status": "up",
      "phys_address": "aabb.cc00.4030",
      "port_channel": {
        "port_channel_member": false
      },
      "port_speed": "auto speed",
      "switchport_enable": false,
      "type": "AmdP2",
      "vrf": "MGMT"
    },
    "Loopback0": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "8.8.8.8/8": {
          "ip": "8.8.8.8",
          "prefix_length": "8",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    },
    "Loopback11": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "88.88.88.88/24": {
          "ip": "88.88.88.88",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    },
    "Loopback201": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "208.1.4.1/24": {
          "ip": "208.1.4.1",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    },
    "Loopback202": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "208.1.5.1/24": {
          "ip": "208.1.5.1",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    },
    "Loopback203": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "208.1.6.1/24": {
          "ip": "208.1.6.1",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    },
    "Loopback204": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "208.1.7.1/24": {
          "ip": "208.1.7.1",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    }
  }
}