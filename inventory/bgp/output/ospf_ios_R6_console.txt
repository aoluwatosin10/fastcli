+++ R6: executing command 'show ip ospf' +++
show ip ospf
R6#
+++ R6: executing command 'show ip protocols' +++
show ip protocols
*** IP Routing is NSF aware ***

Routing Protocol is "application"
  Sending updates every 0 seconds
  Invalid after 0 seconds, hold down 0, flushed after 0
  Outgoing update filter list for all interfaces is not set
  Incoming update filter list for all interfaces is not set
  Maximum path: 32
  Routing for Networks:
  Routing Information Sources:
    Gateway         Distance      Last Update
  Distance: (default is 4)

Routing Protocol is "bgp 600"
  Outgoing update filter list for all interfaces is not set
  Incoming update filter list for all interfaces is not set
  IGP synchronization is disabled
  Automatic route summarization is disabled
  Neighbor(s):
    Address          FiltIn FiltOut DistIn DistOut Weight RouteMap
    192.1.36.3                                           
    192.1.67.7                                           
  Maximum path: 1
  Routing Information Sources:
    Gateway         Distance      Last Update
    192.1.67.7            20      00:12:13
    192.1.36.3            20      00:12:43
  Distance: external 20 internal 200 local 200

R6#
+++ R6: executing command 'show ip ospf mpls ldp interface' +++
show ip ospf mpls ldp interface
R6#
+++ R6: executing command 'show ip ospf mpls traffic-eng link' +++
show ip ospf mpls traffic-eng link
R6#
+++ R6: executing command 'show ip ospf database router' +++
show ip ospf database router
R6#
+++ R6: executing command 'show ip ospf database network' +++
show ip ospf database network
R6#
+++ R6: executing command 'show ip ospf database external' +++
show ip ospf database external
R6#
+++ R6: executing command 'show ip ospf database summary' +++
show ip ospf database summary
R6#
+++ R6: executing command 'show ip ospf database opaque-area' +++
show ip ospf database opaque-area
R6#
+++ R6: executing command 'show ip ospf virtual-links' +++
show ip ospf virtual-links
R6#
+++ R6: executing command 'show ip ospf interface' +++
show ip ospf interface
R6#
+++ R6: executing command 'show ip ospf neighbor detail' +++
show ip ospf neighbor detail
R6#
+++ R6: executing command 'show ip ospf sham-links' +++
show ip ospf sham-links
R6#
rser.ios.show_ospf.ShowIpOspfDatabaseOpaqueArea'>
Show Command: show ip ospf database opaque-area
Parser Output is empty
Could not learn <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfVirtualLinks'>
Show Command: show ip ospf virtual-links
Parser Output is empty
Could not learn <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfInterface'>
Show Command: show ip ospf interface 
Parser Output is empty
Could not learn <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfNeighborDetail'>
Show Command: show ip ospf neighbor detail
Parser Output is empty
Could not learn <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfShamLinks'>
Show Command: show ip ospf sham-links
Parser Output is empty
+====================================================================================================================================================+
| Commands for learning feature 'Ospf'                                                                                                               |
+====================================================================================================================================================+
| - Parsed commands                                                                                                                                  |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.iosxe.show_protocols.ShowIpProtocols'>, arguments: {'vrf':''}                                                     |
|====================================================================================================================================================|
| - Commands with empty output                                                                                                                       |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspf'>                                                                                        |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfMplsLdpInterface'>, arguments: {'interface':''}                                           |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfMplsTrafficEngLink'>                                                                      |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfDatabaseRouter'>                                                                          |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfDatabaseNetwork'>                                                                         |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfDatabaseExternal'>                                                                        |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfDatabaseSummary'>                                                                         |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfDatabaseOpaqueArea'>                                                                      |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfVirtualLinks'>                                                                            |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfInterface'>, arguments: {'interface':''}                                                  |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfNeighborDetail'>, arguments: {'neighbor':''}                                              |
|   cmd: <class 'genie.libs.parser.ios.show_ospf.ShowIpOspfShamLinks'>                                                                               |
|====================================================================================================================================================|
