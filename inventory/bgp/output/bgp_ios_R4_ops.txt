{
  "_exclude": [
    "if_handle",
    "keepalives",
    "last_reset",
    "reset_reason",
    "foreign_port",
    "local_port",
    "msg_rcvd",
    "msg_sent",
    "up_down",
    "bgp_table_version",
    "routing_table_version",
    "tbl_ver",
    "table_version",
    "memory_usage",
    "updates",
    "mss",
    "total",
    "total_bytes",
    "up_time",
    "bgp_negotiated_keepalive_timers",
    "hold_time",
    "keepalive_interval",
    "sent",
    "received",
    "status_codes",
    "holdtime",
    "router_id",
    "connections_dropped",
    "connections_established",
    "advertised",
    "prefixes",
    "routes",
    "state_pfxrcd"
  ],
  "attributes": null,
  "commands": null,
  "connections": null,
  "context_manager": {},
  "info": {
    "instance": {
      "default": {
        "bgp_id": 400,
        "vrf": {
          "default": {
            "cluster_id": "192.1.14.4",
            "neighbor": {
              "192.1.14.1": {
                "address_family": {
                  "ipv4 unicast": {
                    "bgp_table_version": 8,
                    "path": {
                      "memory_usage": 924,
                      "total_entries": 11
                    },
                    "prefixes": {
                      "memory_usage": 1008,
                      "total_entries": 7
                    },
                    "routing_table_version": 8,
                    "total_memory": 3196
                  }
                },
                "bgp_negotiated_capabilities": {
                  "enhanced_refresh": "advertised and received",
                  "four_octets_asn": "advertised and received",
                  "route_refresh": "advertised and received(new)",
                  "stateful_switchover": "NO for session 1"
                },
                "bgp_negotiated_keepalive_timers": {
                  "hold_time": 180,
                  "keepalive_interval": 60
                },
                "bgp_neighbor_counters": {
                  "messages": {
                    "received": {
                      "keepalives": 15,
                      "notifications": 0,
                      "opens": 1,
                      "updates": 5
                    },
                    "sent": {
                      "keepalives": 15,
                      "notifications": 0,
                      "opens": 1,
                      "updates": 6
                    }
                  }
                },
                "bgp_session_transport": {
                  "connection": {
                    "last_reset": "never",
                    "state": "Established"
                  },
                  "transport": {
                    "foreign_host": "192.1.14.1",
                    "foreign_port": "179",
                    "local_host": "192.1.14.4",
                    "local_port": "35403",
                    "mss": 1460
                  }
                },
                "bgp_version": 4,
                "remote_as": 1000,
                "session_state": "Established",
                "shutdown": false
              },
              "192.1.47.7": {
                "address_family": {
                  "ipv4 unicast": {
                    "bgp_table_version": 8,
                    "path": {
                      "memory_usage": 924,
                      "total_entries": 11
                    },
                    "prefixes": {
                      "memory_usage": 1008,
                      "total_entries": 7
                    },
                    "routing_table_version": 8,
                    "total_memory": 3196
                  }
                },
                "bgp_negotiated_capabilities": {
                  "enhanced_refresh": "advertised and received",
                  "four_octets_asn": "advertised and received",
                  "route_refresh": "advertised and received(new)",
                  "stateful_switchover": "NO for session 1"
                },
                "bgp_negotiated_keepalive_timers": {
                  "hold_time": 180,
                  "keepalive_interval": 60
                },
                "bgp_neighbor_counters": {
                  "messages": {
                    "received": {
                      "keepalives": 15,
                      "notifications": 0,
                      "opens": 1,
                      "updates": 6
                    },
                    "sent": {
                      "keepalives": 15,
                      "notifications": 0,
                      "opens": 1,
                      "updates": 6
                    }
                  }
                },
                "bgp_session_transport": {
                  "connection": {
                    "last_reset": "never",
                    "state": "Established"
                  },
                  "transport": {
                    "foreign_host": "192.1.47.7",
                    "foreign_port": "179",
                    "local_host": "192.1.47.4",
                    "local_port": "34073",
                    "mss": 1460
                  }
                },
                "bgp_version": 4,
                "remote_as": 700,
                "session_state": "Established",
                "shutdown": false
              }
            }
          },
          "mgmt": {
            "cluster_id": "192.1.14.4"
          }
        }
      }
    }
  },
  "routes_per_peer": {
    "instance": {
      "default": {
        "vrf": {
          "default": {
            "neighbor": {
              "192.1.14.1": {
                "address_family": {
                  "ipv4 unicast": {
                    "advertised": {
                      "1.0.0.0": {
                        "index": {
                          "1": {
                            "metric": 0,
                            "next_hop": "192.1.14.1",
                            "origin_codes": "i",
                            "path": "1000",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      },
                      "11.11.11.0/24": {
                        "index": {
                          "1": {
                            "metric": 0,
                            "next_hop": "192.1.14.1",
                            "origin_codes": "i",
                            "path": "1000",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      },
                      "3.0.0.0": {
                        "index": {
                          "1": {
                            "next_hop": "192.1.47.7",
                            "origin_codes": "i",
                            "path": "700 1000",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      },
                      "4.0.0.0": {
                        "index": {
                          "1": {
                            "localprf": 0,
                            "next_hop": "0.0.0.0",
                            "origin_codes": "i",
                            "status_codes": "*>",
                            "weight": 32768
                          }
                        }
                      },
                      "44.44.0.0/16": {
                        "index": {
                          "1": {
                            "localprf": 0,
                            "next_hop": "0.0.0.0",
                            "origin_codes": "i",
                            "status_codes": "*>",
                            "weight": 32768
                          }
                        }
                      },
                      "6.0.0.0": {
                        "index": {
                          "1": {
                            "next_hop": "192.1.47.7",
                            "origin_codes": "i",
                            "path": "700 600",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      },
                      "7.0.0.0": {
                        "index": {
                          "1": {
                            "metric": 0,
                            "next_hop": "192.1.47.7",
                            "origin_codes": "i",
                            "path": "700",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      }
                    },
                    "input_queue": 0,
                    "msg_rcvd": 23,
                    "msg_sent": 24,
                    "output_queue": 0,
                    "routes": {
                      "1.0.0.0": {
                        "index": {
                          "1": {
                            "metric": 0,
                            "next_hop": "192.1.14.1",
                            "origin_codes": "i",
                            "path": "1000",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      },
                      "11.11.11.0/24": {
                        "index": {
                          "1": {
                            "metric": 0,
                            "next_hop": "192.1.14.1",
                            "origin_codes": "i",
                            "path": "1000",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      },
                      "6.0.0.0": {
                        "index": {
                          "1": {
                            "next_hop": "192.1.14.1",
                            "origin_codes": "i",
                            "path": "1000 700 600",
                            "status_codes": "*",
                            "weight": 0
                          }
                        }
                      },
                      "7.0.0.0": {
                        "index": {
                          "1": {
                            "next_hop": "192.1.14.1",
                            "origin_codes": "i",
                            "path": "1000 700",
                            "status_codes": "*",
                            "weight": 0
                          }
                        }
                      }
                    },
                    "state_pfxrcd": "4",
                    "tbl_ver": 8,
                    "up_down": "00:12:43"
                  }
                },
                "remote_as": 1000
              },
              "192.1.47.7": {
                "address_family": {
                  "ipv4 unicast": {
                    "advertised": {
                      "1.0.0.0": {
                        "index": {
                          "1": {
                            "metric": 0,
                            "next_hop": "192.1.14.1",
                            "origin_codes": "i",
                            "path": "1000",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      },
                      "11.11.11.0/24": {
                        "index": {
                          "1": {
                            "metric": 0,
                            "next_hop": "192.1.14.1",
                            "origin_codes": "i",
                            "path": "1000",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      },
                      "3.0.0.0": {
                        "index": {
                          "1": {
                            "next_hop": "192.1.47.7",
                            "origin_codes": "i",
                            "path": "700 1000",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      },
                      "4.0.0.0": {
                        "index": {
                          "1": {
                            "localprf": 0,
                            "next_hop": "0.0.0.0",
                            "origin_codes": "i",
                            "status_codes": "*>",
                            "weight": 32768
                          }
                        }
                      },
                      "44.44.0.0/16": {
                        "index": {
                          "1": {
                            "localprf": 0,
                            "next_hop": "0.0.0.0",
                            "origin_codes": "i",
                            "status_codes": "*>",
                            "weight": 32768
                          }
                        }
                      },
                      "6.0.0.0": {
                        "index": {
                          "1": {
                            "next_hop": "192.1.47.7",
                            "origin_codes": "i",
                            "path": "700 600",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      },
                      "7.0.0.0": {
                        "index": {
                          "1": {
                            "metric": 0,
                            "next_hop": "192.1.47.7",
                            "origin_codes": "i",
                            "path": "700",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      }
                    },
                    "input_queue": 0,
                    "msg_rcvd": 24,
                    "msg_sent": 22,
                    "output_queue": 0,
                    "routes": {
                      "1.0.0.0": {
                        "index": {
                          "1": {
                            "next_hop": "192.1.47.7",
                            "origin_codes": "i",
                            "path": "700 1000",
                            "status_codes": "*",
                            "weight": 0
                          }
                        }
                      },
                      "11.11.11.0/24": {
                        "index": {
                          "1": {
                            "next_hop": "192.1.47.7",
                            "origin_codes": "i",
                            "path": "700 1000",
                            "status_codes": "*",
                            "weight": 0
                          }
                        }
                      },
                      "3.0.0.0": {
                        "index": {
                          "1": {
                            "next_hop": "192.1.47.7",
                            "origin_codes": "i",
                            "path": "700 1000",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      },
                      "6.0.0.0": {
                        "index": {
                          "1": {
                            "next_hop": "192.1.47.7",
                            "origin_codes": "i",
                            "path": "700 600",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      },
                      "7.0.0.0": {
                        "index": {
                          "1": {
                            "metric": 0,
                            "next_hop": "192.1.47.7",
                            "origin_codes": "i",
                            "path": "700",
                            "status_codes": "*>",
                            "weight": 0
                          }
                        }
                      }
                    },
                    "state_pfxrcd": "5",
                    "tbl_ver": 8,
                    "up_down": "00:12:46"
                  }
                },
                "remote_as": 700
              }
            }
          }
        }
      }
    }
  },
  "table": {
    "instance": {
      "default": {
        "vrf": {
          "default": {
            "address_family": {
              "ipv4 unicast": {
                "prefixes": {
                  "1.0.0.0/8": {
                    "index": {
                      "1": {
                        "gateway": "192.1.47.7",
                        "localpref": 100,
                        "next_hop": "192.1.47.7",
                        "origin_codes": "i",
                        "originator": "192.1.17.7",
                        "status_codes": "* ",
                        "update_group": 1
                      },
                      "2": {
                        "gateway": "192.1.14.1",
                        "localpref": 100,
                        "metric": 0,
                        "next_hop": "192.1.14.1",
                        "origin_codes": "i",
                        "originator": "192.1.12.1",
                        "status_codes": "*>",
                        "update_group": 1
                      }
                    },
                    "paths": "2 available, best #2, table default",
                    "table_version": "5"
                  },
                  "11.11.11.0/24": {
                    "index": {
                      "1": {
                        "gateway": "192.1.47.7",
                        "localpref": 100,
                        "next_hop": "192.1.47.7",
                        "origin_codes": "i",
                        "originator": "192.1.17.7",
                        "status_codes": "* ",
                        "update_group": 1
                      },
                      "2": {
                        "gateway": "192.1.14.1",
                        "localpref": 100,
                        "metric": 0,
                        "next_hop": "192.1.14.1",
                        "origin_codes": "i",
                        "originator": "192.1.12.1",
                        "status_codes": "*>",
                        "update_group": 1
                      }
                    },
                    "paths": "2 available, best #2, table default",
                    "table_version": "6"
                  },
                  "3.0.0.0/8": {
                    "index": {
                      "1": {
                        "gateway": "192.1.47.7",
                        "localpref": 100,
                        "next_hop": "192.1.47.7",
                        "origin_codes": "i",
                        "originator": "192.1.17.7",
                        "status_codes": "*>",
                        "update_group": 1
                      }
                    },
                    "paths": "1 available, best #1, table default",
                    "table_version": "7"
                  },
                  "4.0.0.0/8": {
                    "index": {
                      "1": {
                        "gateway": "0.0.0.0",
                        "localpref": 100,
                        "metric": 0,
                        "next_hop": "0.0.0.0",
                        "origin_codes": "i",
                        "originator": "192.1.14.4",
                        "status_codes": "*>",
                        "update_group": 1,
                        "weight": "32768"
                      }
                    },
                    "paths": "1 available, best #1, table default",
                    "table_version": "2"
                  },
                  "44.44.0.0/16": {
                    "index": {
                      "1": {
                        "gateway": "0.0.0.0",
                        "localpref": 100,
                        "metric": 0,
                        "next_hop": "0.0.0.0",
                        "origin_codes": "i",
                        "originator": "192.1.14.4",
                        "status_codes": "*>",
                        "update_group": 1,
                        "weight": "32768"
                      }
                    },
                    "paths": "1 available, best #1, table default",
                    "table_version": "3"
                  },
                  "6.0.0.0/8": {
                    "index": {
                      "1": {
                        "gateway": "192.1.14.1",
                        "localpref": 100,
                        "next_hop": "192.1.14.1",
                        "origin_codes": "i",
                        "originator": "192.1.12.1",
                        "status_codes": "* ",
                        "update_group": 1
                      },
                      "2": {
                        "gateway": "192.1.47.7",
                        "localpref": 100,
                        "next_hop": "192.1.47.7",
                        "origin_codes": "i",
                        "originator": "192.1.17.7",
                        "status_codes": "*>",
                        "update_group": 1
                      }
                    },
                    "paths": "2 available, best #2, table default",
                    "table_version": "8"
                  },
                  "7.0.0.0/8": {
                    "index": {
                      "1": {
                        "gateway": "192.1.14.1",
                        "localpref": 100,
                        "next_hop": "192.1.14.1",
                        "origin_codes": "i",
                        "originator": "192.1.12.1",
                        "status_codes": "* ",
                        "update_group": 1
                      },
                      "2": {
                        "gateway": "192.1.47.7",
                        "localpref": 100,
                        "metric": 0,
                        "next_hop": "192.1.47.7",
                        "origin_codes": "i",
                        "originator": "192.1.17.7",
                        "status_codes": "*>",
                        "update_group": 1
                      }
                    },
                    "paths": "2 available, best #2, table default",
                    "table_version": "4"
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}