+++ R6: executing command 'show bgp all summary' +++
show bgp all summary
For address family: IPv4 Unicast
BGP router identifier 192.1.36.6, local AS number 600
BGP table version is 8, main routing table version 8
7 network entries using 1008 bytes of memory
11 path entries using 924 bytes of memory
7/5 BGP path/bestpath attribute entries using 1120 bytes of memory
6 BGP AS-PATH entries using 144 bytes of memory
0 BGP route-map cache entries using 0 bytes of memory
0 BGP filter-list cache entries using 0 bytes of memory
BGP using 3196 total bytes of memory
BGP activity 7/0 prefixes, 11/0 paths, scan interval 60 secs

Neighbor        V           AS MsgRcvd MsgSent   TblVer  InQ OutQ Up/Down  State/PfxRcd
192.1.36.3      4         1000      21      22        8    0    0 00:12:47        4
192.1.67.7      4          700      24      24        8    0    0 00:12:44        6
R6#
+++ R6: executing command 'show ip bgp template peer-session' +++
show ip bgp template peer-session
No templates configured

R6#
+++ R6: executing command 'show ip bgp template peer-policy' +++
show ip bgp template peer-policy
No templates configured

R6#
+++ R6: executing command 'show vrf detail | inc \(VRF' +++
show vrf detail | inc \(VRF
VRF mgmt (VRF Id = 1); default RD <not set>; default VPNID <not set>
R6#
+++ R6: executing command 'show bgp all cluster-ids' +++
show bgp all cluster-ids
Global cluster-id: 192.1.36.6 (configured: 0.0.0.0)
BGP client-to-client reflection:         Configured    Used
  all (inter-cluster and intra-cluster): ENABLED
  intra-cluster:                         ENABLED       ENABLED

List of cluster-ids:
Cluster-id     #-neighbors C2C-rfl-CFG C2C-rfl-USE
R6#
+++ R6: executing command 'show ip bgp all dampening parameters' +++
show ip bgp all dampening parameters
For address family: IPv4 Unicast

% dampening not enabled for base

For address family: IPv4 Multicast

% dampening not enabled for base

For address family: L2VPN E-VPN

% dampening not enabled for base

For address family: MVPNv4 Unicast

% dampening not enabled for base
R6#
+++ R6: executing command 'show bgp all neighbors' +++
show bgp all neighbors
For address family: IPv4 Unicast
BGP neighbor is 192.1.36.3,  remote AS 1000, external link
  BGP version 4, remote router ID 192.1.23.3
  BGP state = Established, up for 00:12:48
  Last read 00:00:12, last write 00:00:14, hold time is 180, keepalive interval is 60 seconds
  Neighbor sessions:
    1 active, is not multisession capable (disabled)
  Neighbor capabilities:
    Route refresh: advertised and received(new)
    Four-octets ASN Capability: advertised and received
    Address family IPv4 Unicast: advertised and received
    Enhanced Refresh Capability: advertised and received
    Multisession Capability: 
    Stateful switchover support enabled: NO for session 1
  Message statistics:
    InQ depth is 0
    OutQ depth is 0
    
                         Sent       Rcvd
    Opens:                  1          1
    Notifications:          0          0
    Updates:                6          5
    Keepalives:            15         15
    Route Refresh:          0          0
    Total:                 22         21
  Do log neighbor state changes (via global configuration)
  Default minimum time between advertisement runs is 30 seconds

  Address tracking is enabled, the RIB does have a route to 192.1.36.3
  Route to peer address reachability Up: 1; Down: 0
    Last notification 00:12:50
  Connections established 1; dropped 0
  Last reset never
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
  Interface associated: Serial1/0 (peering address in same link)
  Transport(tcp) path-mtu-discovery is enabled
  Graceful-Restart is disabled
  SSO is disabled
Connection state is ESTAB, I/O status: 1, unread input bytes: 0            
Connection is ECN Disabled, Mininum incoming TTL 0, Outgoing TTL 1
Local host: 192.1.36.6, Local port: 34705
Foreign host: 192.1.36.3, Foreign port: 179
Connection tableid (VRF): 0
Maximum output segment queue size: 50

Enqueued packets for retransmit: 0, input: 0  mis-ordered: 0 (0 bytes)

Event Timers (current time is 0x522335):
Timer          Starts    Wakeups            Next
Retrans            20          1             0x0
TimeWait            0          0             0x0
AckHold            17         14             0x0
SendWnd             0          0             0x0
KeepAlive           0          0             0x0
GiveUp              0          0             0x0
PmtuAger           98         97        0x5228B5
DeadWait            0          0             0x0
Linger              0          0             0x0
ProcessQ            0          0             0x0

iss: 1995955856  snduna: 1995956485  sndnxt: 1995956485
irs: 2394611839  rcvnxt: 2394612411

sndwnd:  15756  scale:      0  maxrcvwnd:  16384
rcvwnd:  15813  scale:      0  delrcvwnd:    571

SRTT: 909 ms, RTTO: 1600 ms, RTV: 691 ms, KRTT: 0 ms
minRTT: 8 ms, maxRTT: 1000 ms, ACK hold: 200 ms
uptime: 770294 ms, Sent idletime: 12467 ms, Receive idletime: 12670 ms 
Status Flags: active open
Option Flags: nagle, path mtu capable
IP Precedence value : 6

Datagrams (max data segment is 1460 bytes):
Rcvd: 35 (out of order: 0), with data: 18, total data bytes: 571
Sent: 37 (retransmit: 1, fastretransmit: 0, partialack: 0, Second Congestion: 0), with data: 19, total data bytes: 628

 Packets received in fast path: 0, fast processed: 0, slow path: 0
 fast lock acquisition failures: 0, slow path: 0
TCP Semaphore      0xC724D424  FREE 

BGP neighbor is 192.1.67.7,  remote AS 700, external link
  BGP version 4, remote router ID 192.1.17.7
  BGP state = Established, up for 00:12:45
  Last read 00:00:40, last write 00:00:03, hold time is 180, keepalive interval is 60 seconds
  Neighbor sessions:
    1 active, is not multisession capable (disabled)
  Neighbor capabilities:
    Route refresh: advertised and received(new)
    Four-octets ASN Capability: advertised and received
    Address family IPv4 Unicast: advertised and received
    Enhanced Refresh Capability: advertised and received
    Multisession Capability: 
    Stateful switchover support enabled: NO for session 1
  Message statistics:
    InQ depth is 0
    OutQ depth is 0
    
                         Sent       Rcvd
    Opens:                  1          1
    Notifications:          0          0
    Updates:                6          6
    Keepalives:            15         15
    Route Refresh:          0          0
    Total:                 24         24
  Do log neighbor state changes (via global configuration)
  Default minimum time between advertisement runs is 30 seconds

  Address tracking is enabled, the RIB does have a route to 192.1.67.7
  Route to peer address reachability Up: 1; Down: 0
    Last notification 00:12:50
  Connections established 1; dropped 0
  Last reset never
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
  Interface associated: Serial1/1 (peering address in same link)
  Transport(tcp) path-mtu-discovery is enabled
  Graceful-Restart is disabled
  SSO is disabled
Connection state is ESTAB, I/O status: 1, unread input bytes: 0            
Connection is ECN Disabled, Mininum incoming TTL 0, Outgoing TTL 1
Local host: 192.1.67.6, Local port: 28771
Foreign host: 192.1.67.7, Foreign port: 179
Connection tableid (VRF): 0
Maximum output segment queue size: 50

Enqueued packets for retransmit: 0, input: 0  mis-ordered: 0 (0 bytes)

Event Timers (current time is 0x522335):
Timer          Starts    Wakeups            Next
Retrans            19          0             0x0
TimeWait            0          0             0x0
AckHold            17         13             0x0
SendWnd             0          0             0x0
KeepAlive           0          0             0x0
GiveUp              0          0             0x0
PmtuAger          100         99        0x52275E
DeadWait            0          0             0x0
Linger              0          0             0x0
ProcessQ            0          0             0x0

iss: 4206182986  snduna: 4206183661  sndnxt: 4206183661
irs:  682599048  rcvnxt:  682599715

sndwnd:  15710  scale:      0  maxrcvwnd:  16384
rcvwnd:  15718  scale:      0  delrcvwnd:    666

SRTT: 921 ms, RTTO: 1531 ms, RTV: 610 ms, KRTT: 0 ms
minRTT: 6 ms, maxRTT: 1000 ms, ACK hold: 200 ms
uptime: 765165 ms, Sent idletime: 3595 ms, Receive idletime: 3386 ms 
Status Flags: active open
Option Flags: nagle, path mtu capable
IP Precedence value : 6

Datagrams (max data segment is 1460 bytes):
Rcvd: 38 (out of order: 0), with data: 19, total data bytes: 666
Sent: 38 (retransmit: 0, fastretransmit: 0, partialack: 0, Second Congestion: 0), with data: 20, total data bytes: 674

 Packets received in fast path: 0, fast processed: 0, slow path: 0
 fast lock acquisition failures: 0, slow path: 0
TCP Semaphore      0xC768ADF4  FREE 


For address family: IPv4 Multicast

For address family: L2VPN E-VPN

For address family: MVPNv4 Unicast
R6#
+++ R6: executing command 'show bgp all neighbors 192.1.36.3 policy' +++
show bgp all neighbors 192.1.36.3 policy
 Neighbor: 192.1.36.3, Address-Family: IPv4 Unicast
R6#
+++ R6: executing command 'show bgp all neighbors 192.1.67.7 policy' +++
show bgp all neighbors 192.1.67.7 policy
 Neighbor: 192.1.67.7, Address-Family: IPv4 Unicast
R6#
+++ R6: executing command 'show bgp all' +++
show bgp all
For address family: IPv4 Unicast

BGP table version is 8, local router ID is 192.1.36.6
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal, 
              r RIB-failure, S Stale, m multipath, b backup-path, f RT-Filter, 
              x best-external, a additional-path, c RIB-compressed, 
              t secondary path, 
Origin codes: i - IGP, e - EGP, ? - incomplete
RPKI validation codes: V valid, I invalid, N Not found

     Network          Next Hop            Metric LocPrf Weight Path
 *>   1.0.0.0          192.1.67.7                             0 700 1000 i
 *    3.0.0.0          192.1.67.7                             0 700 1000 i
 *>                    192.1.36.3               0             0 1000 i
 *    4.0.0.0          192.1.36.3                             0 1000 700 400 i
 *>                    192.1.67.7                             0 700 400 i
 *>   6.0.0.0          0.0.0.0                  0         32768 i
 *    7.0.0.0          192.1.36.3                             0 1000 700 i
 *>                    192.1.67.7               0             0 700 i
 *>   11.11.11.0/24    192.1.67.7                             0 700 1000 i
 *    44.44.0.0/16     192.1.36.3                             0 1000 700 400 i
 *>                    192.1.67.7                             0 700 400 i

For address family: IPv4 Multicast


For address family: L2VPN E-VPN


For address family: MVPNv4 Unicast

R6#
+++ R6: executing command 'show bgp all detail' +++
show bgp all detail
For address family: IPv4 Unicast

BGP routing table entry for 1.0.0.0/8, version 5
  Paths: (1 available, best #1, table default)
  Advertised to update-groups:
     1         
  Refresh Epoch 2
  700 1000
    192.1.67.7 from 192.1.67.7 (192.1.17.7)
      Origin IGP, localpref 100, valid, external, best
      rx pathid: 0, tx pathid: 0x0
BGP routing table entry for 3.0.0.0/8, version 3
  Paths: (2 available, best #2, table default)
  Advertised to update-groups:
     1         
  Refresh Epoch 2
  700 1000
    192.1.67.7 from 192.1.67.7 (192.1.17.7)
      Origin IGP, localpref 100, valid, external
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 1
  1000
    192.1.36.3 from 192.1.36.3 (192.1.23.3)
      Origin IGP, metric 0, localpref 100, valid, external, best
      rx pathid: 0, tx pathid: 0x0
BGP routing table entry for 4.0.0.0/8, version 7
  Paths: (2 available, best #2, table default)
  Advertised to update-groups:
     1         
  Refresh Epoch 1
  1000 700 400
    192.1.36.3 from 192.1.36.3 (192.1.23.3)
      Origin IGP, localpref 100, valid, external
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 2
  700 400
    192.1.67.7 from 192.1.67.7 (192.1.17.7)
      Origin IGP, localpref 100, valid, external, best
      rx pathid: 0, tx pathid: 0x0
BGP routing table entry for 6.0.0.0/8, version 2
  Paths: (1 available, best #1, table default)
  Advertised to update-groups:
     1         
  Refresh Epoch 1
  Local
    0.0.0.0 from 0.0.0.0 (192.1.36.6)
      Origin IGP, metric 0, localpref 100, weight 32768, valid, sourced, local, best
      rx pathid: 0, tx pathid: 0x0
BGP routing table entry for 7.0.0.0/8, version 4
  Paths: (2 available, best #2, table default)
  Advertised to update-groups:
     1         
  Refresh Epoch 1
  1000 700
    192.1.36.3 from 192.1.36.3 (192.1.23.3)
      Origin IGP, localpref 100, valid, external
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 2
  700
    192.1.67.7 from 192.1.67.7 (192.1.17.7)
      Origin IGP, metric 0, localpref 100, valid, external, best
      rx pathid: 0, tx pathid: 0x0
BGP routing table entry for 11.11.11.0/24, version 6
  Paths: (1 available, best #1, table default)
  Advertised to update-groups:
     1         
  Refresh Epoch 2
  700 1000
    192.1.67.7 from 192.1.67.7 (192.1.17.7)
      Origin IGP, localpref 100, valid, external, best
      rx pathid: 0, tx pathid: 0x0
BGP routing table entry for 44.44.0.0/16, version 8
  Paths: (2 available, best #2, table default)
  Advertised to update-groups:
     1         
  Refresh Epoch 1
  1000 700 400
    192.1.36.3 from 192.1.36.3 (192.1.23.3)
      Origin IGP, localpref 100, valid, external
      rx pathid: 0, tx pathid: 0
  Refresh Epoch 2
  700 400
    192.1.67.7 from 192.1.67.7 (192.1.17.7)
      Origin IGP, localpref 100, valid, external, best
      rx pathid: 0, tx pathid: 0x0

For address family: IPv4 Multicast


For address family: L2VPN E-VPN


For address family: MVPNv4 Unicast

R6#
+++ R6: executing command 'show bgp all neighbors 192.1.36.3 advertised-routes' +++
show bgp all neighbors 192.1.36.3 advertised-routes
For address family: IPv4 Unicast
BGP table version is 8, local router ID is 192.1.36.6
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal, 
              r RIB-failure, S Stale, m multipath, b backup-path, f RT-Filter, 
              x best-external, a additional-path, c RIB-compressed, 
              t secondary path, 
Origin codes: i - IGP, e - EGP, ? - incomplete
RPKI validation codes: V valid, I invalid, N Not found

     Network          Next Hop            Metric LocPrf Weight Path
 *>   1.0.0.0          192.1.67.7                             0 700 1000 i
 *>   3.0.0.0          192.1.36.3               0             0 1000 i
 *>   4.0.0.0          192.1.67.7                             0 700 400 i
 *>   6.0.0.0          0.0.0.0                  0         32768 i
 *>   7.0.0.0          192.1.67.7               0             0 700 i
 *>   11.11.11.0/24    192.1.67.7                             0 700 1000 i
 *>   44.44.0.0/16     192.1.67.7                             0 700 400 i

Total number of prefixes 7 
R6#
+++ R6: executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 192.1.36.3,  remote AS 1000, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
BGP neighbor is 192.1.67.7,  remote AS 700, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
R6#
+++ R6: executing command 'show bgp all neighbors 192.1.67.7 advertised-routes' +++
show bgp all neighbors 192.1.67.7 advertised-routes
For address family: IPv4 Unicast
BGP table version is 8, local router ID is 192.1.36.6
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal, 
              r RIB-failure, S Stale, m multipath, b backup-path, f RT-Filter, 
              x best-external, a additional-path, c RIB-compressed, 
              t secondary path, 
Origin codes: i - IGP, e - EGP, ? - incomplete
RPKI validation codes: V valid, I invalid, N Not found

     Network          Next Hop            Metric LocPrf Weight Path
 *>   1.0.0.0          192.1.67.7                             0 700 1000 i
 *>   3.0.0.0          192.1.36.3               0             0 1000 i
 *>   4.0.0.0          192.1.67.7                             0 700 400 i
 *>   6.0.0.0          0.0.0.0                  0         32768 i
 *>   7.0.0.0          192.1.67.7               0             0 700 i
 *>   11.11.11.0/24    192.1.67.7                             0 700 1000 i
 *>   44.44.0.0/16     192.1.67.7                             0 700 400 i

Total number of prefixes 7 
R6#
+++ R6: executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 192.1.36.3,  remote AS 1000, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
BGP neighbor is 192.1.67.7,  remote AS 700, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
R6#
+++ R6: executing command 'show bgp all neighbors 192.1.36.3 routes' +++
show bgp all neighbors 192.1.36.3 routes
For address family: IPv4 Unicast
BGP table version is 8, local router ID is 192.1.36.6
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal, 
              r RIB-failure, S Stale, m multipath, b backup-path, f RT-Filter, 
              x best-external, a additional-path, c RIB-compressed, 
              t secondary path, 
Origin codes: i - IGP, e - EGP, ? - incomplete
RPKI validation codes: V valid, I invalid, N Not found

     Network          Next Hop            Metric LocPrf Weight Path
 *>   3.0.0.0          192.1.36.3               0             0 1000 i
 *    4.0.0.0          192.1.36.3                             0 1000 700 400 i
 *    7.0.0.0          192.1.36.3                             0 1000 700 i
 *    44.44.0.0/16     192.1.36.3                             0 1000 700 400 i

Total number of prefixes 4 
R6#
+++ R6: executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 192.1.36.3,  remote AS 1000, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
BGP neighbor is 192.1.67.7,  remote AS 700, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
R6#
+++ R6: executing command 'show bgp all neighbors 192.1.67.7 routes' +++
show bgp all neighbors 192.1.67.7 routes
For address family: IPv4 Unicast
BGP table version is 8, local router ID is 192.1.36.6
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal, 
              r RIB-failure, S Stale, m multipath, b backup-path, f RT-Filter, 
              x best-external, a additional-path, c RIB-compressed, 
              t secondary path, 
Origin codes: i - IGP, e - EGP, ? - incomplete
RPKI validation codes: V valid, I invalid, N Not found

     Network          Next Hop            Metric LocPrf Weight Path
 *>   1.0.0.0          192.1.67.7                             0 700 1000 i
 *    3.0.0.0          192.1.67.7                             0 700 1000 i
 *>   4.0.0.0          192.1.67.7                             0 700 400 i
 *>   7.0.0.0          192.1.67.7               0             0 700 i
 *>   11.11.11.0/24    192.1.67.7                             0 700 1000 i
 *>   44.44.0.0/16     192.1.67.7                             0 700 400 i

Total number of prefixes 6 
R6#
+++ R6: executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 192.1.36.3,  remote AS 1000, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
BGP neighbor is 192.1.67.7,  remote AS 700, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
R6#
+++ R6: executing command 'show bgp all neighbors 192.1.36.3 received-routes' +++
show bgp all neighbors 192.1.36.3 received-routes
For address family: IPv4 Unicast
% Inbound soft reconfiguration not enabled on 192.1.36.3
R6#
+++ R6: executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 192.1.36.3,  remote AS 1000, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
BGP neighbor is 192.1.67.7,  remote AS 700, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
R6#
+++ R6: executing command 'show bgp all neighbors 192.1.67.7 received-routes' +++
show bgp all neighbors 192.1.67.7 received-routes
For address family: IPv4 Unicast
% Inbound soft reconfiguration not enabled on 192.1.67.7
R6#
+++ R6: executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 192.1.36.3,  remote AS 1000, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
BGP neighbor is 192.1.67.7,  remote AS 700, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
R6#
Could not learn <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsReceivedRoutes'>
Show Command: show bgp all neighbors 192.1.67.7 received-routes
Parser Output is empty
+====================================================================================================================================================+
| Commands for learning feature 'Bgp'                                                                                                                |
+====================================================================================================================================================+
| - Parsed commands                                                                                                                                  |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllSummary'>, arguments: {'address_family':'','vrf':''}                                       |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllClusterIds'>                                                                               |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighbors'>, arguments: {'address_family':'','neighbor':''}                                |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAll'>, arguments: {'address_family':''}                                                       |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllDetail'>, arguments: {'address_family':'','vrf':''}                                        |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsAdvertisedRoutes'>, arguments: {'address_family':'','neighbor':'192.1.36.3'}      |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsAdvertisedRoutes'>, arguments: {'address_family':'','neighbor':'192.1.67.7'}      |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsRoutes'>, arguments: {'address_family':'','neighbor':'192.1.36.3'}                |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsRoutes'>, arguments: {'address_family':'','neighbor':'192.1.67.7'}                |
|====================================================================================================================================================|
| - Commands with empty output                                                                                                                       |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowIpBgpTemplatePeerSession'>                                                                       |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowIpBgpTemplatePeerPolicy'>                                                                        |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowIpBgpAllDampeningParameters'>                                                                    |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsPolicy'>, arguments: {'neighbor':'192.1.36.3'}                                    |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsPolicy'>, arguments: {'neighbor':'192.1.67.7'}                                    |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsReceivedRoutes'>, arguments: {'address_family':'','neighbor':'192.1.36.3'}        |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsReceivedRoutes'>, arguments: {'address_family':'','neighbor':'192.1.67.7'}        |
|====================================================================================================================================================|
