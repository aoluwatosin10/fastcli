{
  "_exclude": [
    "in_discards",
    "in_octets",
    "in_pkts",
    "last_clear",
    "out_octets",
    "out_pkts",
    "in_rate",
    "out_rate",
    "in_errors",
    "in_crc_errors",
    "in_rate_pkts",
    "out_rate_pkts",
    "in_broadcast_pkts",
    "out_broadcast_pkts",
    "in_multicast_pkts",
    "out_multicast_pkts",
    "in_unicast_pkts",
    "out_unicast_pkts",
    "last_change",
    "mac_address",
    "phys_address",
    "((t|T)unnel.*)",
    "(Null.*)",
    "chars_out",
    "chars_in",
    "pkts_out",
    "pkts_in",
    "mgmt0"
  ],
  "attributes": null,
  "commands": null,
  "connections": null,
  "context_manager": {},
  "info": {
    "Ethernet0/0": {
      "bandwidth": 10000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 1000,
      "duplex_mode": "auto",
      "enabled": false,
      "encapsulation": {
        "encapsulation": "arpa"
      },
      "flow_control": {
        "receive": false,
        "send": false
      },
      "mac_address": "aabb.cc00.3a00",
      "mtu": 1500,
      "oper_status": "down",
      "phys_address": "aabb.cc00.3a00",
      "port_channel": {
        "port_channel_member": false
      },
      "port_speed": "auto speed",
      "switchport_enable": false,
      "type": "AmdP2"
    },
    "Ethernet0/1": {
      "bandwidth": 10000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 1000,
      "duplex_mode": "auto",
      "enabled": false,
      "encapsulation": {
        "encapsulation": "arpa"
      },
      "flow_control": {
        "receive": false,
        "send": false
      },
      "mac_address": "aabb.cc00.3a10",
      "mtu": 1500,
      "oper_status": "down",
      "phys_address": "aabb.cc00.3a10",
      "port_channel": {
        "port_channel_member": false
      },
      "port_speed": "auto speed",
      "switchport_enable": false,
      "type": "AmdP2"
    },
    "Ethernet0/2": {
      "bandwidth": 10000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 1000,
      "duplex_mode": "auto",
      "enabled": false,
      "encapsulation": {
        "encapsulation": "arpa"
      },
      "flow_control": {
        "receive": false,
        "send": false
      },
      "mac_address": "aabb.cc00.3a20",
      "mtu": 1500,
      "oper_status": "down",
      "phys_address": "aabb.cc00.3a20",
      "port_channel": {
        "port_channel_member": false
      },
      "port_speed": "auto speed",
      "switchport_enable": false,
      "type": "AmdP2"
    },
    "Ethernet0/3": {
      "accounting": {
        "arp": {
          "chars_in": 9840,
          "chars_out": 1320,
          "pkts_in": 164,
          "pkts_out": 22
        },
        "cdp": {
          "chars_in": 162466,
          "chars_out": 27574,
          "pkts_in": 437,
          "pkts_out": 75
        },
        "dec mop": {
          "chars_in": 4081,
          "chars_out": 770,
          "pkts_in": 53,
          "pkts_out": 10
        },
        "ip": {
          "chars_in": 317737,
          "chars_out": 380224,
          "pkts_in": 2843,
          "pkts_out": 2841
        },
        "other": {
          "chars_in": 1957,
          "chars_out": 32280,
          "pkts_in": 12,
          "pkts_out": 538
        }
      },
      "bandwidth": 10000,
      "counters": {
        "in_broadcast_pkts": 295,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 967,
        "in_octets": 492947,
        "in_pkts": 3458,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 424774,
        "out_pkts": 3435,
        "rate": {
          "in_rate": 4000,
          "in_rate_pkts": 6,
          "load_interval": 300,
          "out_rate": 7000,
          "out_rate_pkts": 6
        }
      },
      "delay": 1000,
      "duplex_mode": "auto",
      "enabled": true,
      "encapsulation": {
        "encapsulation": "arpa"
      },
      "flow_control": {
        "receive": false,
        "send": false
      },
      "ipv4": {
        "192.168.20.174/24": {
          "ip": "192.168.20.174",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mac_address": "aabb.cc00.3a30",
      "mtu": 1500,
      "oper_status": "up",
      "phys_address": "aabb.cc00.3a30",
      "port_channel": {
        "port_channel_member": false
      },
      "port_speed": "auto speed",
      "switchport_enable": false,
      "type": "AmdP2",
      "vrf": "mgmt"
    },
    "Loopback0": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "4.4.4.4/8": {
          "ip": "4.4.4.4",
          "prefix_length": "8",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    },
    "Loopback1": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "44.44.44.44/16": {
          "ip": "44.44.44.44",
          "prefix_length": "16",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    },
    "Loopback11": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "10.4.4.4/24": {
          "ip": "10.4.4.4",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    },
    "Serial1/0": {
      "accounting": {
        "cdp": {
          "chars_in": 7788,
          "chars_out": 8142,
          "pkts_in": 22,
          "pkts_out": 23
        },
        "ip": {
          "chars_in": 2253,
          "chars_out": 2306,
          "pkts_in": 37,
          "pkts_out": 37
        },
        "other": {
          "chars_in": 0,
          "chars_out": 1872,
          "pkts_in": 0,
          "pkts_out": 78
        }
      },
      "bandwidth": 1544,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 101,
        "in_octets": 12267,
        "in_pkts": 138,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 12320,
        "out_pkts": 138,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 20000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "hdlc"
      },
      "ipv4": {
        "192.1.14.4/24": {
          "ip": "192.1.14.4",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mtu": 1500,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "M4T"
    },
    "Serial1/1": {
      "accounting": {
        "cdp": {
          "chars_in": 7434,
          "chars_out": 7788,
          "pkts_in": 21,
          "pkts_out": 22
        },
        "ip": {
          "chars_in": 2254,
          "chars_out": 2216,
          "pkts_in": 36,
          "pkts_out": 36
        },
        "other": {
          "chars_in": 0,
          "chars_out": 1872,
          "pkts_in": 0,
          "pkts_out": 78
        }
      },
      "bandwidth": 1544,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 100,
        "in_octets": 11914,
        "in_pkts": 136,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 11876,
        "out_pkts": 136,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 20000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "hdlc"
      },
      "ipv4": {
        "192.1.47.4/24": {
          "ip": "192.1.47.4",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mtu": 1500,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "M4T"
    },
    "Serial1/2": {
      "bandwidth": 1544,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 20000,
      "enabled": false,
      "encapsulation": {
        "encapsulation": "hdlc"
      },
      "mtu": 1500,
      "oper_status": "down",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "M4T"
    },
    "Serial1/3": {
      "bandwidth": 1544,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 20000,
      "enabled": false,
      "encapsulation": {
        "encapsulation": "hdlc"
      },
      "mtu": 1500,
      "oper_status": "down",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "M4T"
    }
  }
}