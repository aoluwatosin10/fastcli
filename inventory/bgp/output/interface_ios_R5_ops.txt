{
  "_exclude": [
    "in_discards",
    "in_octets",
    "in_pkts",
    "last_clear",
    "out_octets",
    "out_pkts",
    "in_rate",
    "out_rate",
    "in_errors",
    "in_crc_errors",
    "in_rate_pkts",
    "out_rate_pkts",
    "in_broadcast_pkts",
    "out_broadcast_pkts",
    "in_multicast_pkts",
    "out_multicast_pkts",
    "in_unicast_pkts",
    "out_unicast_pkts",
    "last_change",
    "mac_address",
    "phys_address",
    "((t|T)unnel.*)",
    "(Null.*)",
    "chars_out",
    "chars_in",
    "pkts_out",
    "pkts_in",
    "mgmt0"
  ],
  "attributes": null,
  "commands": null,
  "connections": null,
  "context_manager": {},
  "info": {
    "Ethernet0/0": {
      "bandwidth": 10000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 1000,
      "duplex_mode": "auto",
      "enabled": false,
      "encapsulation": {
        "encapsulation": "arpa"
      },
      "flow_control": {
        "receive": false,
        "send": false
      },
      "mac_address": "aabb.cc00.3c00",
      "mtu": 1500,
      "oper_status": "down",
      "phys_address": "aabb.cc00.3c00",
      "port_channel": {
        "port_channel_member": false
      },
      "port_speed": "auto speed",
      "switchport_enable": false,
      "type": "AmdP2"
    },
    "Ethernet0/1": {
      "bandwidth": 10000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 1000,
      "duplex_mode": "auto",
      "enabled": false,
      "encapsulation": {
        "encapsulation": "arpa"
      },
      "flow_control": {
        "receive": false,
        "send": false
      },
      "mac_address": "aabb.cc00.3c10",
      "mtu": 1500,
      "oper_status": "down",
      "phys_address": "aabb.cc00.3c10",
      "port_channel": {
        "port_channel_member": false
      },
      "port_speed": "auto speed",
      "switchport_enable": false,
      "type": "AmdP2"
    },
    "Ethernet0/2": {
      "bandwidth": 10000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 1000,
      "duplex_mode": "auto",
      "enabled": false,
      "encapsulation": {
        "encapsulation": "arpa"
      },
      "flow_control": {
        "receive": false,
        "send": false
      },
      "mac_address": "aabb.cc00.3c20",
      "mtu": 1500,
      "oper_status": "down",
      "phys_address": "aabb.cc00.3c20",
      "port_channel": {
        "port_channel_member": false
      },
      "port_speed": "auto speed",
      "switchport_enable": false,
      "type": "AmdP2"
    },
    "Ethernet0/3": {
      "accounting": {
        "arp": {
          "chars_in": 9840,
          "chars_out": 1320,
          "pkts_in": 164,
          "pkts_out": 22
        },
        "cdp": {
          "chars_in": 163214,
          "chars_out": 27200,
          "pkts_in": 439,
          "pkts_out": 74
        },
        "dec mop": {
          "chars_in": 4235,
          "chars_out": 693,
          "pkts_in": 55,
          "pkts_out": 9
        },
        "ip": {
          "chars_in": 297333,
          "chars_out": 328874,
          "pkts_in": 2511,
          "pkts_out": 2475
        },
        "other": {
          "chars_in": 1506,
          "chars_out": 32280,
          "pkts_in": 10,
          "pkts_out": 538
        }
      },
      "bandwidth": 10000,
      "counters": {
        "in_broadcast_pkts": 295,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 969,
        "in_octets": 472694,
        "in_pkts": 3123,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 369711,
        "out_pkts": 3062,
        "rate": {
          "in_rate": 3000,
          "in_rate_pkts": 5,
          "load_interval": 300,
          "out_rate": 5000,
          "out_rate_pkts": 5
        }
      },
      "delay": 1000,
      "duplex_mode": "auto",
      "enabled": true,
      "encapsulation": {
        "encapsulation": "arpa"
      },
      "flow_control": {
        "receive": false,
        "send": false
      },
      "ipv4": {
        "192.168.20.176/24": {
          "ip": "192.168.20.176",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mac_address": "aabb.cc00.3c30",
      "mtu": 1500,
      "oper_status": "up",
      "phys_address": "aabb.cc00.3c30",
      "port_channel": {
        "port_channel_member": false
      },
      "port_speed": "auto speed",
      "switchport_enable": false,
      "type": "AmdP2",
      "vrf": "mgmt"
    },
    "Loopback0": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "5.5.5.5/8": {
          "ip": "5.5.5.5",
          "prefix_length": "8",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    },
    "Loopback11": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "10.5.5.5/24": {
          "ip": "10.5.5.5",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    },
    "Loopback201": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "205.1.4.1/24": {
          "ip": "205.1.4.1",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    },
    "Loopback202": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "205.1.5.1/24": {
          "ip": "205.1.5.1",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    },
    "Loopback203": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "205.1.6.1/24": {
          "ip": "205.1.6.1",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    },
    "Loopback204": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "205.1.7.1/24": {
          "ip": "205.1.7.1",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    },
    "Serial1/0": {
      "accounting": {
        "cdp": {
          "chars_in": 7788,
          "chars_out": 8142,
          "pkts_in": 22,
          "pkts_out": 23
        },
        "ip": {
          "chars_in": 2024,
          "chars_out": 2028,
          "pkts_in": 36,
          "pkts_out": 36
        },
        "other": {
          "chars_in": 0,
          "chars_out": 1872,
          "pkts_in": 0,
          "pkts_out": 78
        }
      },
      "bandwidth": 1544,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 101,
        "in_octets": 12038,
        "in_pkts": 137,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 12042,
        "out_pkts": 137,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 20000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "hdlc"
      },
      "ipv4": {
        "192.1.25.5/24": {
          "ip": "192.1.25.5",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mtu": 1500,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "M4T"
    },
    "Serial1/1": {
      "bandwidth": 1544,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 20000,
      "enabled": false,
      "encapsulation": {
        "encapsulation": "hdlc"
      },
      "mtu": 1500,
      "oper_status": "down",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "M4T"
    },
    "Serial1/2": {
      "bandwidth": 1544,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 20000,
      "enabled": false,
      "encapsulation": {
        "encapsulation": "hdlc"
      },
      "mtu": 1500,
      "oper_status": "down",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "M4T"
    },
    "Serial1/3": {
      "bandwidth": 1544,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 20000,
      "enabled": false,
      "encapsulation": {
        "encapsulation": "hdlc"
      },
      "mtu": 1500,
      "oper_status": "down",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "M4T"
    }
  }
}