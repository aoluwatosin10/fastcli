+++ R2: executing command 'show bgp all summary' +++
show bgp all summary
For address family: IPv4 Unicast
BGP router identifier 192.1.12.2, local AS number 1000
BGP table version is 3, main routing table version 3
2 network entries using 288 bytes of memory
2 path entries using 168 bytes of memory
2/2 BGP path/bestpath attribute entries using 320 bytes of memory
1 BGP AS-PATH entries using 24 bytes of memory
0 BGP route-map cache entries using 0 bytes of memory
0 BGP filter-list cache entries using 0 bytes of memory
BGP using 800 total bytes of memory
BGP activity 2/0 prefixes, 2/0 paths, scan interval 60 secs

Neighbor        V           AS MsgRcvd MsgSent   TblVer  InQ OutQ Up/Down  State/PfxRcd
10.1.1.1        4         1000       0       0        1    0    0 never    Idle
10.3.3.3        4         1000       0       0        1    0    0 never    Idle
192.1.25.5      4          500      18      19        3    0    0 00:12:47        1
R2#
+++ R2: executing command 'show ip bgp template peer-session' +++
show ip bgp template peer-session
No templates configured

R2#
+++ R2: executing command 'show ip bgp template peer-policy' +++
show ip bgp template peer-policy
No templates configured

R2#
+++ R2: executing command 'show vrf detail | inc \(VRF' +++
show vrf detail | inc \(VRF
VRF mgmt (VRF Id = 1); default RD <not set>; default VPNID <not set>
R2#
+++ R2: executing command 'show bgp all cluster-ids' +++
show bgp all cluster-ids
Global cluster-id: 192.1.12.2 (configured: 0.0.0.0)
BGP client-to-client reflection:         Configured    Used
  all (inter-cluster and intra-cluster): ENABLED
  intra-cluster:                         ENABLED       ENABLED

List of cluster-ids:
Cluster-id     #-neighbors C2C-rfl-CFG C2C-rfl-USE
R2#
+++ R2: executing command 'show ip bgp all dampening parameters' +++
show ip bgp all dampening parameters
For address family: IPv4 Unicast

% dampening not enabled for base

For address family: IPv4 Multicast

% dampening not enabled for base

For address family: L2VPN E-VPN

% dampening not enabled for base

For address family: MVPNv4 Unicast

% dampening not enabled for base
R2#
+++ R2: executing command 'show bgp all neighbors' +++
show bgp all neighbors
For address family: IPv4 Unicast
BGP neighbor is 10.1.1.1,  remote AS 1000, internal link
  BGP version 4, remote router ID 0.0.0.0
  BGP state = Idle
  Neighbor sessions:
    0 active, is not multisession capable (disabled)
    Stateful switchover support enabled: NO
  Do log neighbor state changes (via global configuration)
  Default minimum time between advertisement runs is 0 seconds

  Address tracking is enabled, the RIB does have a route to 10.1.1.1
  Route to peer address reachability Up: 0; Down: 0
    Last notification never
  Connections established 0; dropped 0
  Last reset never
  Interface associated: (none) (peering address NOT in same link)
  Transport(tcp) path-mtu-discovery is enabled
  Graceful-Restart is disabled
  SSO is disabled
  No active TCP connection

BGP neighbor is 10.3.3.3,  remote AS 1000, internal link
  BGP version 4, remote router ID 0.0.0.0
  BGP state = Idle
  Neighbor sessions:
    0 active, is not multisession capable (disabled)
    Stateful switchover support enabled: NO
  Do log neighbor state changes (via global configuration)
  Default minimum time between advertisement runs is 0 seconds

  Address tracking is enabled, the RIB does have a route to 10.3.3.3
  Route to peer address reachability Up: 0; Down: 0
    Last notification never
  Connections established 0; dropped 0
  Last reset never
  Interface associated: (none) (peering address NOT in same link)
  Transport(tcp) path-mtu-discovery is enabled
  Graceful-Restart is disabled
  SSO is disabled
  No active TCP connection

BGP neighbor is 192.1.25.5,  remote AS 500, external link
  BGP version 4, remote router ID 192.1.25.5
  BGP state = Established, up for 00:12:48
  Last read 00:00:48, last write 00:00:04, hold time is 180, keepalive interval is 60 seconds
  Neighbor sessions:
    1 active, is not multisession capable (disabled)
  Neighbor capabilities:
    Route refresh: advertised and received(new)
    Four-octets ASN Capability: advertised and received
    Address family IPv4 Unicast: advertised and received
    Enhanced Refresh Capability: advertised and received
    Multisession Capability: 
    Stateful switchover support enabled: NO for session 1
  Message statistics:
    InQ depth is 0
    OutQ depth is 0
    
                         Sent       Rcvd
    Opens:                  1          1
    Notifications:          0          0
    Updates:                2          2
    Keepalives:            16         15
    Route Refresh:          0          0
    Total:                 19         18
  Do log neighbor state changes (via global configuration)
  Default minimum time between advertisement runs is 30 seconds

  Address tracking is enabled, the RIB does have a route to 192.1.25.5
  Route to peer address reachability Up: 1; Down: 0
    Last notification 00:12:49
  Connections established 1; dropped 0
  Last reset never
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
  Interface associated: Serial1/0 (peering address in same link)
  Transport(tcp) path-mtu-discovery is enabled
  Graceful-Restart is disabled
  SSO is disabled
Connection state is ESTAB, I/O status: 1, unread input bytes: 0            
Connection is ECN Disabled, Mininum incoming TTL 0, Outgoing TTL 1
Local host: 192.1.25.2, Local port: 27464
Foreign host: 192.1.25.5, Foreign port: 179
Connection tableid (VRF): 0
Maximum output segment queue size: 50

Enqueued packets for retransmit: 0, input: 0  mis-ordered: 0 (0 bytes)

Event Timers (current time is 0x521EA3):
Timer          Starts    Wakeups            Next
Retrans            18          0             0x0
TimeWait            0          0             0x0
AckHold            16         13             0x0
SendWnd             0          0             0x0
KeepAlive           0          0             0x0
GiveUp              0          0             0x0
PmtuAger           96         95        0x521FC9
DeadWait            0          0             0x0
Linger              0          0             0x0
ProcessQ            0          0             0x0

iss: 3621204926  snduna: 3621205363  sndnxt: 3621205363
irs:  421263648  rcvnxt:  421264066

sndwnd:  15948  scale:      0  maxrcvwnd:  16384
rcvwnd:  15967  scale:      0  delrcvwnd:    417

SRTT: 909 ms, RTTO: 1600 ms, RTV: 691 ms, KRTT: 0 ms
minRTT: 8 ms, maxRTT: 1000 ms, ACK hold: 200 ms
uptime: 768278 ms, Sent idletime: 4759 ms, Receive idletime: 4559 ms 
Status Flags: active open
Option Flags: nagle, path mtu capable
IP Precedence value : 6

Datagrams (max data segment is 1460 bytes):
Rcvd: 34 (out of order: 0), with data: 17, total data bytes: 417
Sent: 35 (retransmit: 0, fastretransmit: 0, partialack: 0, Second Congestion: 0), with data: 18, total data bytes: 436

 Packets received in fast path: 0, fast processed: 0, slow path: 0
 fast lock acquisition failures: 0, slow path: 0
TCP Semaphore      0xC752621C  FREE 


For address family: IPv4 Multicast

For address family: L2VPN E-VPN

For address family: MVPNv4 Unicast
R2#
+++ R2: executing command 'show bgp all neighbors 10.1.1.1 policy' +++
show bgp all neighbors 10.1.1.1 policy
 Neighbor: 10.1.1.1, Address-Family: IPv4 Unicast
 Locally configured policies:
  route-reflector-client
  next-hop-self
R2#
+++ R2: executing command 'show bgp all neighbors 10.3.3.3 policy' +++
show bgp all neighbors 10.3.3.3 policy
 Neighbor: 10.3.3.3, Address-Family: IPv4 Unicast
 Locally configured policies:
  route-reflector-client
  next-hop-self
R2#
+++ R2: executing command 'show bgp all neighbors 192.1.25.5 policy' +++
show bgp all neighbors 192.1.25.5 policy
 Neighbor: 192.1.25.5, Address-Family: IPv4 Unicast
R2#
+++ R2: executing command 'show bgp all' +++
show bgp all
For address family: IPv4 Unicast

BGP table version is 3, local router ID is 192.1.12.2
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal, 
              r RIB-failure, S Stale, m multipath, b backup-path, f RT-Filter, 
              x best-external, a additional-path, c RIB-compressed, 
              t secondary path, 
Origin codes: i - IGP, e - EGP, ? - incomplete
RPKI validation codes: V valid, I invalid, N Not found

     Network          Next Hop            Metric LocPrf Weight Path
 *>   2.0.0.0          0.0.0.0                  0         32768 i
 *>   5.0.0.0          192.1.25.5               0             0 500 i

For address family: IPv4 Multicast


For address family: L2VPN E-VPN


For address family: MVPNv4 Unicast

R2#
+++ R2: executing command 'show bgp all detail' +++
show bgp all detail
For address family: IPv4 Unicast

BGP routing table entry for 2.0.0.0/8, version 2
  Paths: (1 available, best #1, table default)
  Advertised to update-groups:
     1         
  Refresh Epoch 1
  Local
    0.0.0.0 from 0.0.0.0 (192.1.12.2)
      Origin IGP, metric 0, localpref 100, weight 32768, valid, sourced, local, best
      rx pathid: 0, tx pathid: 0x0
BGP routing table entry for 5.0.0.0/8, version 3
  Paths: (1 available, best #1, table default)
  Not advertised to any peer
  Refresh Epoch 1
  500
    192.1.25.5 from 192.1.25.5 (192.1.25.5)
      Origin IGP, metric 0, localpref 100, valid, external, best
      rx pathid: 0, tx pathid: 0x0

For address family: IPv4 Multicast


For address family: L2VPN E-VPN


For address family: MVPNv4 Unicast

R2#
+++ R2: executing command 'show bgp all neighbors 10.1.1.1 advertised-routes' +++
show bgp all neighbors 10.1.1.1 advertised-routes
For address family: IPv4 Unicast

Total number of prefixes 0 
R2#
+++ R2: executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 10.1.1.1,  remote AS 1000, internal link
BGP neighbor is 10.3.3.3,  remote AS 1000, internal link
BGP neighbor is 192.1.25.5,  remote AS 500, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
R2#
+++ R2: executing command 'show bgp all neighbors 10.3.3.3 advertised-routes' +++
show bgp all neighbors 10.3.3.3 advertised-routes
For address family: IPv4 Unicast

Total number of prefixes 0 
R2#
+++ R2: executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 10.1.1.1,  remote AS 1000, internal link
BGP neighbor is 10.3.3.3,  remote AS 1000, internal link
BGP neighbor is 192.1.25.5,  remote AS 500, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
R2#
+++ R2: executing command 'show bgp all neighbors 192.1.25.5 advertised-routes' +++
show bgp all neighbors 192.1.25.5 advertised-routes
For address family: IPv4 Unicast
BGP table version is 3, local router ID is 192.1.12.2
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal, 
              r RIB-failure, S Stale, m multipath, b backup-path, f RT-Filter, 
              x best-external, a additional-path, c RIB-compressed, 
              t secondary path, 
Origin codes: i - IGP, e - EGP, ? - incomplete
RPKI validation codes: V valid, I invalid, N Not found

     Network          Next Hop            Metric LocPrf Weight Path
 *>   2.0.0.0          0.0.0.0                  0         32768 i

Total number of prefixes 1 
R2#
+++ R2: executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 10.1.1.1,  remote AS 1000, internal link
BGP neighbor is 10.3.3.3,  remote AS 1000, internal link
BGP neighbor is 192.1.25.5,  remote AS 500, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
R2#
+++ R2: executing command 'show bgp all neighbors 10.1.1.1 routes' +++
show bgp all neighbors 10.1.1.1 routes
For address family: IPv4 Unicast

Total number of prefixes 0 
R2#
+++ R2: executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 10.1.1.1,  remote AS 1000, internal link
BGP neighbor is 10.3.3.3,  remote AS 1000, internal link
BGP neighbor is 192.1.25.5,  remote AS 500, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
R2#
+++ R2: executing command 'show bgp all neighbors 10.3.3.3 routes' +++
show bgp all neighbors 10.3.3.3 routes
For address family: IPv4 Unicast

Total number of prefixes 0 
R2#
+++ R2: executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 10.1.1.1,  remote AS 1000, internal link
BGP neighbor is 10.3.3.3,  remote AS 1000, internal link
BGP neighbor is 192.1.25.5,  remote AS 500, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
R2#
+++ R2: executing command 'show bgp all neighbors 192.1.25.5 routes' +++
show bgp all neighbors 192.1.25.5 routes
For address family: IPv4 Unicast
BGP table version is 3, local router ID is 192.1.12.2
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal, 
              r RIB-failure, S Stale, m multipath, b backup-path, f RT-Filter, 
              x best-external, a additional-path, c RIB-compressed, 
              t secondary path, 
Origin codes: i - IGP, e - EGP, ? - incomplete
RPKI validation codes: V valid, I invalid, N Not found

     Network          Next Hop            Metric LocPrf Weight Path
 *>   5.0.0.0          192.1.25.5               0             0 500 i

Total number of prefixes 1 
R2#
+++ R2: executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 10.1.1.1,  remote AS 1000, internal link
BGP neighbor is 10.3.3.3,  remote AS 1000, internal link
BGP neighbor is 192.1.25.5,  remote AS 500, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
R2#
+++ R2: executing command 'show bgp all neighbors 10.1.1.1 received-routes' +++
show bgp all neighbors 10.1.1.1 received-routes
For address family: IPv4 Unicast
% Inbound soft reconfiguration not enabled on 10.1.1.1
R2#
+++ R2: executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 10.1.1.1,  remote AS 1000, internal link
BGP neighbor is 10.3.3.3,  remote AS 1000, internal link
BGP neighbor is 192.1.25.5,  remote AS 500, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
R2#
+++ R2: executing command 'show bgp all neighbors 10.3.3.3 received-routes' +++
show bgp all neighbors 10.3.3.3 received-routes
For address family: IPv4 Unicast
% Inbound soft reconfiguration not enabled on 10.3.3.3
R2#
+++ R2: executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 10.1.1.1,  remote AS 1000, internal link
BGP neighbor is 10.3.3.3,  remote AS 1000, internal link
BGP neighbor is 192.1.25.5,  remote AS 500, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
R2#
+++ R2: executing command 'show bgp all neighbors 192.1.25.5 received-routes' +++
show bgp all neighbors 192.1.25.5 received-routes
For address family: IPv4 Unicast
% Inbound soft reconfiguration not enabled on 192.1.25.5
R2#
+++ R2: executing command 'show bgp all neighbors | i BGP neighbor' +++
show bgp all neighbors | i BGP neighbor
BGP neighbor is 10.1.1.1,  remote AS 1000, internal link
BGP neighbor is 10.3.3.3,  remote AS 1000, internal link
BGP neighbor is 192.1.25.5,  remote AS 500, external link
  External BGP neighbor configured for connected checks (single-hop no-disable-connected-check)
R2#
Could not learn <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsReceivedRoutes'>
Show Command: show bgp all neighbors 192.1.25.5 received-routes
Parser Output is empty
+====================================================================================================================================================+
| Commands for learning feature 'Bgp'                                                                                                                |
+====================================================================================================================================================+
| - Parsed commands                                                                                                                                  |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllSummary'>, arguments: {'address_family':'','vrf':''}                                       |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllClusterIds'>                                                                               |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighbors'>, arguments: {'address_family':'','neighbor':''}                                |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAll'>, arguments: {'address_family':''}                                                       |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllDetail'>, arguments: {'address_family':'','vrf':''}                                        |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsAdvertisedRoutes'>, arguments: {'address_family':'','neighbor':'192.1.25.5'}      |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsRoutes'>, arguments: {'address_family':'','neighbor':'192.1.25.5'}                |
|====================================================================================================================================================|
| - Commands with empty output                                                                                                                       |
|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowIpBgpTemplatePeerSession'>                                                                       |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowIpBgpTemplatePeerPolicy'>                                                                        |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowIpBgpAllDampeningParameters'>                                                                    |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsPolicy'>, arguments: {'neighbor':'10.1.1.1'}                                      |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsPolicy'>, arguments: {'neighbor':'10.3.3.3'}                                      |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsPolicy'>, arguments: {'neighbor':'192.1.25.5'}                                    |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsAdvertisedRoutes'>, arguments: {'address_family':'','neighbor':'10.1.1.1'}        |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsAdvertisedRoutes'>, arguments: {'address_family':'','neighbor':'10.3.3.3'}        |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsRoutes'>, arguments: {'address_family':'','neighbor':'10.1.1.1'}                  |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsRoutes'>, arguments: {'address_family':'','neighbor':'10.3.3.3'}                  |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsReceivedRoutes'>, arguments: {'address_family':'','neighbor':'10.1.1.1'}          |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsReceivedRoutes'>, arguments: {'address_family':'','neighbor':'10.3.3.3'}          |
|   cmd: <class 'genie.libs.parser.ios.show_bgp.ShowBgpAllNeighborsReceivedRoutes'>, arguments: {'address_family':'','neighbor':'192.1.25.5'}        |
|====================================================================================================================================================|
