{
  "_exclude": [
    "in_discards",
    "in_octets",
    "in_pkts",
    "last_clear",
    "out_octets",
    "out_pkts",
    "in_rate",
    "out_rate",
    "in_errors",
    "in_crc_errors",
    "in_rate_pkts",
    "out_rate_pkts",
    "in_broadcast_pkts",
    "out_broadcast_pkts",
    "in_multicast_pkts",
    "out_multicast_pkts",
    "in_unicast_pkts",
    "out_unicast_pkts",
    "last_change",
    "mac_address",
    "phys_address",
    "((t|T)unnel.*)",
    "(Null.*)",
    "chars_out",
    "chars_in",
    "pkts_out",
    "pkts_in",
    "mgmt0"
  ],
  "attributes": null,
  "commands": null,
  "connections": null,
  "context_manager": {},
  "info": {
    "Ethernet0/0": {
      "accounting": {
        "arp": {
          "chars_in": 120,
          "chars_out": 120,
          "pkts_in": 2,
          "pkts_out": 2
        },
        "cdp": {
          "chars_in": 8228,
          "chars_out": 8602,
          "pkts_in": 22,
          "pkts_out": 23
        },
        "dec mop": {
          "chars_in": 77,
          "chars_out": 154,
          "pkts_in": 1,
          "pkts_out": 2
        },
        "other": {
          "chars_in": 0,
          "chars_out": 4680,
          "pkts_in": 0,
          "pkts_out": 78
        }
      },
      "bandwidth": 10000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 25,
        "in_octets": 8425,
        "in_pkts": 25,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 13556,
        "out_pkts": 105,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 1000,
      "duplex_mode": "auto",
      "enabled": true,
      "encapsulation": {
        "encapsulation": "arpa"
      },
      "flow_control": {
        "receive": false,
        "send": false
      },
      "ipv4": {
        "192.1.12.2/24": {
          "ip": "192.1.12.2",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mac_address": "aabb.cc00.3d00",
      "mtu": 1500,
      "oper_status": "up",
      "phys_address": "aabb.cc00.3d00",
      "port_channel": {
        "port_channel_member": false
      },
      "port_speed": "auto speed",
      "switchport_enable": false,
      "type": "AmdP2"
    },
    "Ethernet0/1": {
      "accounting": {
        "arp": {
          "chars_in": 0,
          "chars_out": 120,
          "pkts_in": 0,
          "pkts_out": 2
        },
        "cdp": {
          "chars_in": 8228,
          "chars_out": 8602,
          "pkts_in": 22,
          "pkts_out": 23
        },
        "dec mop": {
          "chars_in": 77,
          "chars_out": 154,
          "pkts_in": 1,
          "pkts_out": 2
        },
        "other": {
          "chars_in": 0,
          "chars_out": 4680,
          "pkts_in": 0,
          "pkts_out": 78
        }
      },
      "bandwidth": 10000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 23,
        "in_octets": 8305,
        "in_pkts": 23,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 13556,
        "out_pkts": 105,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 1000,
      "duplex_mode": "auto",
      "enabled": true,
      "encapsulation": {
        "encapsulation": "arpa"
      },
      "flow_control": {
        "receive": false,
        "send": false
      },
      "ipv4": {
        "192.1.23.2/24": {
          "ip": "192.1.23.2",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mac_address": "aabb.cc00.3d10",
      "mtu": 1500,
      "oper_status": "up",
      "phys_address": "aabb.cc00.3d10",
      "port_channel": {
        "port_channel_member": false
      },
      "port_speed": "auto speed",
      "switchport_enable": false,
      "type": "AmdP2"
    },
    "Ethernet0/2": {
      "bandwidth": 10000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 1000,
      "duplex_mode": "auto",
      "enabled": false,
      "encapsulation": {
        "encapsulation": "arpa"
      },
      "flow_control": {
        "receive": false,
        "send": false
      },
      "mac_address": "aabb.cc00.3d20",
      "mtu": 1500,
      "oper_status": "down",
      "phys_address": "aabb.cc00.3d20",
      "port_channel": {
        "port_channel_member": false
      },
      "port_speed": "auto speed",
      "switchport_enable": false,
      "type": "AmdP2"
    },
    "Ethernet0/3": {
      "accounting": {
        "arp": {
          "chars_in": 9720,
          "chars_out": 1320,
          "pkts_in": 162,
          "pkts_out": 22
        },
        "cdp": {
          "chars_in": 161344,
          "chars_out": 27200,
          "pkts_in": 434,
          "pkts_out": 74
        },
        "dec mop": {
          "chars_in": 3927,
          "chars_out": 770,
          "pkts_in": 51,
          "pkts_out": 10
        },
        "ip": {
          "chars_in": 376058,
          "chars_out": 498145,
          "pkts_in": 3650,
          "pkts_out": 3891
        },
        "other": {
          "chars_in": 3607,
          "chars_out": 32280,
          "pkts_in": 18,
          "pkts_out": 538
        }
      },
      "bandwidth": 10000,
      "counters": {
        "in_broadcast_pkts": 295,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 966,
        "in_octets": 551702,
        "in_pkts": 4267,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 543405,
        "out_pkts": 4486,
        "rate": {
          "in_rate": 1000,
          "in_rate_pkts": 2,
          "load_interval": 300,
          "out_rate": 2000,
          "out_rate_pkts": 2
        }
      },
      "delay": 1000,
      "duplex_mode": "auto",
      "enabled": true,
      "encapsulation": {
        "encapsulation": "arpa"
      },
      "flow_control": {
        "receive": false,
        "send": false
      },
      "ipv4": {
        "192.168.20.172/24": {
          "ip": "192.168.20.172",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mac_address": "aabb.cc00.3d30",
      "mtu": 1500,
      "oper_status": "up",
      "phys_address": "aabb.cc00.3d30",
      "port_channel": {
        "port_channel_member": false
      },
      "port_speed": "auto speed",
      "switchport_enable": false,
      "type": "AmdP2",
      "vrf": "mgmt"
    },
    "Loopback0": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "2.2.2.2/8": {
          "ip": "2.2.2.2",
          "prefix_length": "8",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    },
    "Loopback11": {
      "bandwidth": 8000000,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 5000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "loopback"
      },
      "ipv4": {
        "10.2.2.2/24": {
          "ip": "10.2.2.2",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mtu": 1514,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "Loopback"
    },
    "Serial1/0": {
      "accounting": {
        "cdp": {
          "chars_in": 7788,
          "chars_out": 8142,
          "pkts_in": 22,
          "pkts_out": 23
        },
        "ip": {
          "chars_in": 2028,
          "chars_out": 2024,
          "pkts_in": 36,
          "pkts_out": 36
        },
        "other": {
          "chars_in": 0,
          "chars_out": 1872,
          "pkts_in": 0,
          "pkts_out": 78
        }
      },
      "bandwidth": 1544,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 100,
        "in_octets": 11688,
        "in_pkts": 136,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 12038,
        "out_pkts": 137,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 20000,
      "enabled": true,
      "encapsulation": {
        "encapsulation": "hdlc"
      },
      "ipv4": {
        "192.1.25.2/24": {
          "ip": "192.1.25.2",
          "prefix_length": "24",
          "secondary": false
        }
      },
      "mtu": 1500,
      "oper_status": "up",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "M4T"
    },
    "Serial1/1": {
      "bandwidth": 1544,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 20000,
      "enabled": false,
      "encapsulation": {
        "encapsulation": "hdlc"
      },
      "mtu": 1500,
      "oper_status": "down",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "M4T"
    },
    "Serial1/2": {
      "bandwidth": 1544,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 20000,
      "enabled": false,
      "encapsulation": {
        "encapsulation": "hdlc"
      },
      "mtu": 1500,
      "oper_status": "down",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "M4T"
    },
    "Serial1/3": {
      "bandwidth": 1544,
      "counters": {
        "in_broadcast_pkts": 0,
        "in_crc_errors": 0,
        "in_errors": 0,
        "in_multicast_pkts": 0,
        "in_octets": 0,
        "in_pkts": 0,
        "last_clear": "never",
        "out_errors": 0,
        "out_octets": 0,
        "out_pkts": 0,
        "rate": {
          "in_rate": 0,
          "in_rate_pkts": 0,
          "load_interval": 300,
          "out_rate": 0,
          "out_rate_pkts": 0
        }
      },
      "delay": 20000,
      "enabled": false,
      "encapsulation": {
        "encapsulation": "hdlc"
      },
      "mtu": 1500,
      "oper_status": "down",
      "port_channel": {
        "port_channel_member": false
      },
      "switchport_enable": false,
      "type": "M4T"
    }
  }
}